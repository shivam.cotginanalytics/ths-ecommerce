/* FIXED HEADER */
$(window).scroll(function () {
  if ($(window).scrollTop() > 200) {
    $(".header").addClass("fixed");
  } else {
    $(".header").removeClass("fixed");
  }
});

// small device 
if(screen.width < 992){
  $(".nav-link").click(function(){
    $(this).siblings(".dropdown-menu").toggle();
  });
  $('.inner-drop').on('click', function(){
    $(this).siblings('.inner-dropdown').slideToggle();
  });
}
/* SLIDE FORM */
$(".lets-talk").click(function () {
  $(".sidebar-contact").toggleClass("active");
  $(".toggle").toggleClass("active");
});
$(".sidebar-contact .btn-close").click(function () {
  $(".sidebar-contact").removeClass("active");
});


/* share btn */
$('.share-now').on('click', function(){
    $('.share-on').fadeToggle();
});




$(".product-listing-slider").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000
});

$(".related-product-slider").slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});

// For increment and decrement Product quentity Start=================================
const incrementButton = document.getElementById('increment');
const decrementButton = document.getElementById('decrement');
const display_price = document.getElementById('display_price');

const incrementButton1 = document.getElementById('increment1');
const decrementButton1 = document.getElementById('decrement1');

const counterDisplay = document.getElementById('counterDisplay');
const counterDisplay1 = document.getElementById('counterDisplay1');

const productPrice= $('#product_price').val();
let count = 1;
let count1 = 1;
incrementButton.onclick = () => {
  count = count + 1;
  const total_price=productPrice*count;
  if(total_price>3000){
    $('#price_limit').modal('show');
    return false;
  }
  $('#total').val(total_price);
  display_price.innerHTML='$ '+total_price;
  counterDisplay.innerHTML = count;
}
decrementButton.onclick = () => {
  if (count != 1) {
    counterDisplay.innerHTML = --count;
    const total_price=productPrice*count;
    if(total_price>3000){
      $('#price_limit').modal('show');
      return false;
    }
    $('#total').val(total_price);
    display_price.innerHTML='$ '+total_price;
  }
}
// For increment and decrement Product quentity Start==================================


// For increment and decrement miltipal Product quentity Start=========================
function decrement_product(id){
  const productPrice= $('#product_price'+id).val();
  const counterDisplay = document.getElementById('counterDisplay'+id);
  const display_price = document.getElementById('display_price'+id);
  const total_amount = document.getElementById('total_amount');
  let decrementCount = counterDisplay.innerHTML;
  if (decrementCount != 1) {
    decrementCount--;
    const total_price=productPrice*decrementCount;
    if(total_price>3000){
    $('.order-button').addClass('d-none');
    $('.inquiry-button').removeClass('d-none');
      // return false;
    }else{
      $('.order-button').removeClass('d-none');
      $('.inquiry-button').addClass('d-none');
  }
    product_quantity(id,total_price,decrementCount);

    display_price.innerHTML='$ '+total_price.toLocaleString();
    counterDisplay.innerHTML = decrementCount;
    var amount=parseInt(total_amount.innerHTML.replace('$','').replace(',','')) - parseInt(productPrice);
    total_amount.innerHTML = '$ '+amount.toLocaleString(); 
  }
}

function increment_product(id){
  const productPrice= $('#product_price'+id).val();
  const counterDisplay = document.getElementById('counterDisplay'+id);
  const display_price = document.getElementById('display_price'+id);
  const total_amount = document.getElementById('total_amount');
  let incrementCount = counterDisplay.innerHTML;
  incrementCount++;
  const total_price=productPrice*incrementCount;
  if(total_price>3000){
    $('#price_limit').modal('show');
    $('.order-button').addClass('d-none');
    $('.inquiry-button').removeClass('d-none');
    // return false;
  }else{
    $('.order-button').removeClass('d-none');
    $('.inquiry-button').addClass('d-none');
  }
  product_quantity(id,total_price,incrementCount);

  display_price.innerHTML='$ '+total_price.toLocaleString();
  counterDisplay.innerHTML = incrementCount; 
  var amount=parseInt(total_amount.innerHTML.replace('$','').replace(',','')) + parseInt(productPrice);
  total_amount.innerHTML = '$ '+amount.toLocaleString(); 
}
// For increment and decrement miltipal Product quentity End===========================

// For store Product quentity in db Start==============================================
function product_quantity(id,price,qty){
  var data = new FormData();
  data.append('p_id', id);
  data.append('price', price);
  data.append('qty', qty);

  let url=$('#add_p_quentity_url').val();

  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
  $.ajax({
    url: url,
    type: "POST",
    data: data,
    contentType: false,
    processData: false,
    success: function(response) {
        
    },
    error: function(xhr, textStatus, errorThrown) {
        // Handle error response
    }
  });
}
// For store Product quentity in db End================================================

// For Apply Coupon Start==============================================================
function apply_coupon(){
  const coupon_code=$('#coupon_code').val();
  const url=$('#apply_coupon_url').val();
  var data=new FormData();
  data.append('coupon',coupon_code);
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: url,
    type: "POST",
    data: data,
    contentType: false,
    processData: false,
    success: function(response) {
      if(response.status == true){
        let value=response.result.replace('%','');
        let total_price=$('#total_price').html().replace('$','').replace(',','');
        let discounted_price= total_price - (total_price * (value / 100));
        let discount= total_price-discounted_price;
        $('#discount').html('$ '+discount.toFixed(2).toLocaleString());
        $('#total_price').html('$ '+discounted_price.toFixed(2).toLocaleString());
        $('#payble_amount').val(discounted_price);
        $('#code').val(coupon_code);
        // var message='Applied!';
        $('#coupon_message').html(' ');
        // $('#coupon_message').addClass('text-success');
        // $('#coupon_message').removeClass('text-danger');
        $('#applied_message').removeClass('d-none');
        $('.message').html(coupon_code);
      }else{
        var message='This coupon code is not valid.';
        $('#coupon_message').html(message);
        $('#coupon_message').removeClass('text-success');
        $('#coupon_message').addClass('text-danger');
      }
    },
    error: function(xhr, textStatus, errorThrown) {
        // Handle error response
    }
  });
}
// For Apply Coupon End==================================================================

function coupon_close(){
  location.reload();
};


// incrementButton1.onclick = () => {
//   count1 = count1 + 1;
//   counterDisplay1.innerHTML = count1;
// }
// decrementButton1.onclick = () => {
//   if (count1 != 1) {
//     counterDisplay1.innerHTML = --count1;
//   }
// }

function DeleteCartProduct(id) {
  jQuery('#pqty').val(0);
  var d=1;
  add_to_cart(id,d);
}

function UpdateQty(id,price) {
  var qty = jQuery('#qty'+id).val();
  jQuery('#pqty').val(qty);
  add_to_cart(id);
  jQuery('#total_price_'+id).html('Rs '+qty*price);
}             

function home_add_to_cart(id) {
  jQuery('#product_id').val(id);
  add_to_cart(id);
}

// function add_to_cart(id) {
//   jQuery('#add_to_cart_msg').html('');
//   jQuery('#product_id').val(id);
//   jQuery('#pqty').val(jQuery('#counterDisplay').text());
//   jQuery.ajax({
//     url:'/add_to_cart',
//     data:jQuery('#frmAddToCart').serialize(),
//     type:'POST',
//     success:function(result) {
//       var totalPrice = 0;
//       alert('Product '+result.msg);
//       if(result.totalItem==0) {
//         jQuery('.counter').html('0');
//         jQuery('.add-cart-box').remove();
//       } else {
//         jQuery('.counter').html(result.totalItem);
//         var html = '';
//         jQuery.each(result.data, function(key,val) {
//         totalPrice = parseInt(totalPrice)+(parseInt(val.qty)*parseInt(val.price));
//         html+='<div class="add-view-cart d-flex"><div class="cart_product_image"><a href="javascript:void(0)" class="product-images"><img src="'+PRODUCT_IMAGE+'/'+val.image+'" style="width:40px;" alt="img"></a></div><div class="cart-product-title"><h5><a href="javascript:void(0)">'+val.name+'</a></h5><p class="price_and_qu"><span>'+val.qty+' x </span> '+val.price+'</p></div></div>';
//         });
//       }
//       html+='<div class="subtotal-price"><ul><li>Subtotal : <span class="red-color">'+totalPrice+'</span></li></ul></div>';

//       html+='<div class="btn-box"><a class="contact-button butn me-3" href="">View Cart</a><a class="call-button butn" href="#">Buy Now</a></div>';
//       jQuery('.aa-cartbox-summary').html(html);
//     }
//   });
// }
