<?php

function prx($arr) {
    echo "<pre>";
    print_r($arr);
    die();
}

function getUserTempId() { 
    if(!session()->has('USER_TEMP_ID')) {
        $rand = rand(111111111,999999999);
        session()->put('USER_TEMP_ID',$rand);
        return $rand;
    } else {
        return session()->get('USER_TEMP_ID');
    }

}

function get_product(){
        $all_added_product = session('cart');
        $product_array=[];
        $data = DB::table('products')
        ->join('rams', 'rams.id', '=', 'products.ram')
        ->join('storages', 'storages.id', '=', 'products.storage')
        ->join('brands', 'brands.id', '=', 'products.brand')
        ->join('processors', 'processors.id', '=', 'products.processor')
        ->where(['products.status'=>1])
        ->get(['products.*','rams.size','storages.storage','brands.name as bname','processors.processor']);
        if($all_added_product){
        foreach($all_added_product as $id){
            foreach($data as $value){
                if($value->id == $id){
                    $product_array[]=$value;
                }
            }
        
        }   
        $result['product']=$product_array;
}
return $product_array;
}

function getAddToCartTotalItem() {

    if(session()->has('FRONT_USER_LOGIN')) {
        $uid = session()->get('FRONT_USER_LOGIN');
        $user_type = "Reg";
    } else {
        $uid = getUserTempId();
        $user_type = "Not-Reg";
    }

    $result = DB::table('cart')
            ->leftJoin('products','products.id','=','cart.product_id')
            ->join('rams', 'rams.id', '=', 'products.ram')
            ->join('storages', 'storages.id', '=', 'products.storage')
            ->join('brands', 'brands.id', '=', 'products.brand')
            ->join('processors', 'processors.id', '=', 'products.processor')
            ->where(['user_id'=>$uid])
            ->where(['user_type'=>$user_type])
            ->get(['products.*','rams.size','cart.qty','storages.storage','brands.name as bname','processors.processor']);
    // $result=session()->get('cart');
    
    return $result;
}

function get_catrgory(){
    $res=DB::table('categories')->where(['status'=>1])->get();
    return $res;
}
function get_single_heading($id){
    $res=DB::table('specification_heading')->where('id',$id)->first();
    return $res;
}

function get_cart($id){
    $res=DB::table('cart')->where(['product_id'=>$id,'user_id'=>session()->getId()])->first();
    return $res;
}
function get_user_card(){
    if(session()->getId()){
    $res=DB::table('cart')->where(['user_id'=>session()->getId()])->get();
    return $res;
    }else{
        return false;
    }
    
}
function get_product_by_id($id){
    // $res=DB::table('products')->where(['id'=>$id])->first();
    $res = DB::table('products')
        ->join('rams', 'rams.id', '=', 'products.ram')
        ->join('storages', 'storages.id', '=', 'products.storage')
        ->join('brands', 'brands.id', '=', 'products.brand')
        ->join('processors', 'processors.id', '=', 'products.processor')
        ->where(['products.status'=>1])->where(['products.id'=>$id])
        ->first(['products.*','rams.size','storages.storage','brands.name as bname','processors.processor']);
    return $res;
}

?>