<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Crypt;
use Mail;
use Session;

class FrontController extends Controller
{
    public function index(Request $request,$cat=null)
    {
        $result['home_banners'] = DB::table('home_banners')
                    ->where(['status'=>1])
                    ->get();

        if($cat){ //If use click on category
            $cat_id=DB::table('categories')->where('category_slug',$cat)->first();
            $result['home_products'] = DB::table('products')
                    ->where(['category_id'=>$cat_id->id,'status'=>1])
                    ->get();
        }else{
        $result['home_products'] = DB::table('products')
                    ->where(['status'=>1])
                    ->get();
        }

        $result['category'] = DB::table('categories')
                    ->where(['status'=>1])
                    ->where(['is_home'=>1])
                    ->get();

        $all_added_product = session('cart');
        $product_array=[];
        $data = DB::table('products')
        ->join('rams', 'rams.id', '=', 'products.ram')
        ->join('storages', 'storages.id', '=', 'products.storage')
        ->join('brands', 'brands.id', '=', 'products.brand')
        ->join('processors', 'processors.id', '=', 'products.processor')
        ->where(['products.status'=>1])
        ->get(['products.*','rams.size','storages.storage','brands.name as bname','processors.processor']);
        if($all_added_product){
        foreach($all_added_product as $id){
            foreach($data as $value){
                if($value->id == $id){
                    $product_array[]=$value;
                }
            }
        
        }   
        $result['product']=$product_array;
    }else{
        $result['product']=[];
    }      
        return view('product-listing',$result);  
    }

    public function product(Request $request,$slug)
    {
        $product_id=DB::table('products')->where('slug',$slug)->first();
        $result['product'] = DB::table('products')
            ->join('rams', 'rams.id', '=', 'products.ram')
            ->join('storages', 'storages.id', '=', 'products.storage')
            ->join('brands', 'brands.id', '=', 'products.brand')
            ->join('processors', 'processors.id', '=', 'products.processor')
            ->where(['products.status'=>1])
            ->where(['products.slug'=>$slug])
            ->get(['products.*','rams.size','storages.storage','brands.name as bname','processors.processor']);

        $result['recommend_product'] = DB::table('products')
        ->join('rams', 'rams.id', '=', 'products.ram')
        ->join('storages', 'storages.id', '=', 'products.storage')
        ->join('brands', 'brands.id', '=', 'products.brand')
        ->join('processors', 'processors.id', '=', 'products.processor')
        ->where(['products.status'=>1])
        ->get(['products.*','rams.size','storages.storage','brands.name as bname','processors.processor']);
        foreach($result['product'] as $list1) {
            $result['product_images'][$list1->id] = DB::table('products_image')
                ->where(['products_image.products_id'=>$list1->id])
                ->get();
        }

        $result['specifications'] =DB::table('product_specification')->where('product_id',$product_id->id)->get()->groupBy(function($data) {
            return $data->heading_id;
        });

        return view('product-detail',$result); 
    }

    // public function add_to_cart(Request $request) 
    // {
    //     if($request->session()->has('FRONT_USER_LOGIN')) {
    //         $uid = $request->session()->get('FRONT_USER_LOGIN');
    //         $user_type = "Reg";
    //     } else {
    //         $uid = getUserTempId();
    //         $user_type = "Not-Reg";
    //     }

    //     $pqty = $request->post('pqty');
    //     $product_id = $request->post('product_id');

    //     $check = DB::table('cart')
    //         ->where(['user_id'=>$uid])
    //         ->where(['user_type'=>$user_type])
    //         ->where(['product_id'=>$product_id])
    //         ->get();
    //     if(isset($check[0])) {
    //         $update_id = $check[0]->id;
    //         if($pqty==0){
    //             DB::table('cart')
    //                 ->where(['id'=>$update_id])
    //                 ->delete();
    //             $msg = "Removed";
    //         } else {
    //             DB::table('cart')
    //                 ->where(['id'=>$update_id])
    //                 ->update(['qty'=>$pqty]);
    //             $msg = "Updated";
    //         }
                            
    //     } else {
    //         $id = DB::table('cart')->insertGetId([
    //             'user_id'=>$uid,
    //             'user_type'=>$user_type,
    //             'qty'=>$pqty,
    //             'product_id'=>$product_id
    //         ]);
    //         $msg = "Added"; 
    //     }

    //     $result = DB::table('cart')
    //         ->leftJoin('products','products.id','=','cart.product_id')
    //         ->join('rams', 'rams.id', '=', 'products.ram')
    //         ->join('storages', 'storages.id', '=', 'products.storage')
    //         ->join('brands', 'brands.id', '=', 'products.brand')
    //         ->join('processors', 'processors.id', '=', 'products.processor')
    //         ->where(['user_id'=>$uid])
    //         ->where(['user_type'=>$user_type])
    //         ->get(['products.*','rams.size','cart.qty','storages.storage','brands.name as bname','processors.processor']);

    //     return response()->json(['msg'=>$msg,'data'=>$result,'totalItem'=>count($result)]);
    // $mybrowser = session()->getId();
    // }
    public function add_to_cart(Request $request){
        $product_id=$request->id;
        $product=DB::table('products')->where('id',$product_id)->first();
        $all_added_product=[];
        try{
            $cart=DB::table('cart')->where(['product_id'=>$request->id,'user_id'=>session()->getId()])->first();
            $all_added_product = session('cart');
            if($request->delete){
                // foreach($all_added_product as $key=>$val){
                //     if($val == $request->id){
                //         unset($all_added_product[$key]);        
                //         session()->put('cart', $all_added_product); 
                //         DB::table('cart')->where(['product_id'=>$product_id,'user_id'=>session()->getId()])->delete();
                //     }else{
                        DB::table('cart')->where(['id'=>$request->id,'user_id'=>session()->getId()])->delete();
                    // }
                // }
            }else{
                // if(!$all_added_product){
                //     $all_added_product=[];
                // }
                if(!$cart){
                $data=[
                    'user_id'=>session()->getId(),
                    'qty'=>1,
                    'product_id'=>$product_id,
                    'product_price'=>$product->price,
                ];
                DB::table('cart')->insert($data);
                // session()->push('cart', $product_id);
                }
            }
            $count_cart=DB::table('cart')->where(['user_id'=>session()->getId()])->get();

            $all_added_product = session('cart');
            $product_array=[];
            $product='';
            $data = DB::table('products')
            ->join('rams', 'rams.id', '=', 'products.ram')
            ->join('storages', 'storages.id', '=', 'products.storage')
            ->join('brands', 'brands.id', '=', 'products.brand')
            ->join('processors', 'processors.id', '=', 'products.processor')
            ->where(['products.status'=>1])
            ->get(['products.*','rams.size','storages.storage','brands.name as bname','processors.processor']);
            
            if($count_cart){
            foreach($count_cart as $val){
                foreach($data as $value){
                    if($value->id == $val->product_id){
                        $product_array[]=$value;
                    }
                }
            
            }   
        }
        $response=[
            'status'=>true,
            'message'=>'Product Add Success!',
            'product_count'=>count($count_cart),
            'product'=>$product_array,
        ];
        return $response;
    }catch(\Exception $exception){
        return view('errors.commonError');
    }
    }

    public function product_quantity(Request $request){
        $data=[
            'qty'=>$request->qty,
            'qty_price'=>$request->price,
        ];
        DB::table('cart')->where(['user_id'=>session()->getId(),'product_id'=>$request->p_id])->update($data);
        return true;
    }

    public function biling_page(Request $request)
    {
        $type=0;
        if($request->isMethod('post')){
        if($request->qty){
        $data=[
            'user_id'=>session()->getId(),
            'qty'=>$request->qty,
            'product_id'=>$request->p_id,
            'product_price'=>$request->price,
            'qty_price'=>$request->total_price,
        ];
        $check=DB::table('cart')->where(['user_id'=>session()->getId(),'product_id'=>$request->p_id])->count();
        if($check==0){
        DB::table('cart')->insert($data);
        }
       }
    }else{
        if($request->type){
        $type=1;
    }
    }
        return view('billing-page',compact('type')); 
    }

    public function make_payment(Request $request){
        
        $validate=[ //validate billing address
            'fname'=>'required',
            'region'=>'required',
            'street_address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'pin_code'=>'required',
            'email'=>'required',
            'agree'=>'required',
        ];

        //Payment with card
        if($request->payment_type == 1){
            $validate['card_number']='required';
            $validate['exp_date']='required';
            $validate['cvv']='required';
        }
        $request->validate($validate,[
            'fname.required' => 'The first name field is required.',
        ]);

        $data=[
            'first_name'=>$request->fname,
            'last_name'=>$request->lname,
            'company_region'=>$request->region,
            'street_address'=>$request->street_address,
            'city'=>$request->city,
            'state'=>$request->state,
            'pin_code'=>$request->pin_code,
            'email'=>$request->email,
            'company_name'=>$request->company,
            'order_note'=>$request->order_note,
            'terms'=>$request->agree
        ];
        $result=DB::table('billing_address')->insert($data);
        dd($result);
    }

    public function check_coupon(Request $request){
        $coupon=$request->coupon;
        $find_coupon=DB::table('coupons')->where('code',$coupon)->first();
        if($find_coupon){
            $response=[
                'status'=>true,
                'result'=>$find_coupon->value,
            ];
        }else{
            $response=[
                'status'=>false,
                'result'=>'',
            ];
        }
        return $response;
    }

   
}
