<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\PaymentLogs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\Invoice;
use Mail;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use DB;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function store_enquire(Request $request){
        $validate=[ //validate billing address
            'fname'=>'required',
            'region'=>'required',
            'street_address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'pin_code'=>'required',
            'email'=>'required',
        ];
        $request->validate($validate,[
            'fname.required' => 'The first name field is required.',
        ]);
        $data=[
            'first_name'=>$request->fname,
            'last_name'=>$request->lname,
            'company_region'=>$request->region,
            'street_address'=>$request->street_address,
            'city'=>$request->city,
            'state'=>$request->state,
            'pin_code'=>$request->pin_code,
            'email'=>$request->email,
            'company_name'=>$request->company,
            'product'=>json_encode($request->product),
            'terms'=>$request->agree
        ];
        $result=DB::table('enquire')->insertGetId($data);
        return redirect()->route('thank-you');
    }

    public function check_price($data){
        $product=json_decode($data['product']);
        $paybal_amount=$data['payble_amount'];

        $tax=DB::table('taxes')->where('status',1)->first();    
        
        $price=$tax->tax_value;
        foreach($product as $val){
            $pro=DB::table('products')->where('id',$val->product_id)->first();
            $price=$price+$pro->price;
        }
        $p=(int)$price;
        if($paybal_amount == $price){
            return 'True';
        }else{
            if($data['code']){
                $coupon=DB::table('coupons')->where('code',$data['code'])->first();
                $c=str_replace('%','',$coupon->value);
                $dis_price=$p - $p * $c / 100;
                if($paybal_amount == $dis_price){
                    return true;
                }else{
                    return false;
                }
        }
        }
    }

    public function handleonlinepay(Request $request) {
        $input = $request->input();
       
        $validate=[ //validate billing address
            'fname'=>'required',
            'region'=>'required',
            'street_address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'pin_code'=>'required',
            'email'=>'required',
            'agree'=>'required',
        ];
        

        //Payment with card
        if($request->payment_type == 1){
            $validate['card_number']='required';
            $validate['month']='required';
            $validate['year']='required';
            $validate['cvv']='required';
        }
        $request->validate($validate,[
            'fname.required' => 'The first name field is required.',
        ]);
        $input['card_number']=str_replace('-','',$input['card_number']);

        $check_price=$this->check_price($input);
        if($check_price != true){
            return back()->with('error_msg','Somthin went wrong!');
            return false;
        }
        $data=[
            'first_name'=>$request->fname,
            'last_name'=>$request->lname,
            'company_region'=>$request->region,
            'street_address'=>$request->street_address,
            'city'=>$request->city,
            'state'=>$request->state,
            'pin_code'=>$request->pin_code,
            'email'=>$request->email,
            'company_name'=>$request->company,
            'order_note'=>$request->order_note,
            'terms'=>$request->agree
        ];
        $result=DB::table('billing_address')->insertGetId($data);
        $input['amount']=$request->payble_amount;
        /* Create a merchantAuthenticationType object with authentication details
          retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(env('MERCHANT_LOGIN_ID'));
        $merchantAuthentication->setTransactionKey(env('MERCHANT_TRANSACTION_KEY'));

        // Set the transaction's refId
        $refId = 'ref' . time();
        $cardNumber = preg_replace('/\s+/', '', $input['card_number']);
        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($cardNumber);
        $creditCard->setExpirationDate($input['year'] . "-" .$input['month']);
        $creditCard->setCardCode($input['cvv']);

        // Add the payment data to a paymentType object
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);


        // Set the customer's Bill To address
        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setFirstName($request->fname);
        $customerAddress->setLastName($request->lname);
        $customerAddress->setCompany($request->company);
        $customerAddress->setAddress($request->street_address);
        $customerAddress->setCity($request->city);
        $customerAddress->setState($request->state);
        $customerAddress->setZip($request->pin_code);
        $customerAddress->setCountry($request->region);

        // Set the customer's identifying information
        $customerData = new AnetAPI\CustomerDataType();
        $customerData->setType("individual");
        $customerData->setId($refId);
        $customerData->setEmail($request->email);

        // Add values for transaction settings
        $duplicateWindowSetting = new AnetAPI\SettingType();
        $duplicateWindowSetting->setSettingName("duplicateWindow");
        $duplicateWindowSetting->setSettingValue("60");

        // Add some merchant defined fields. These fields won't be stored with the transaction,
        // but will be echoed back in the response.
        $merchantDefinedField1 = new AnetAPI\UserFieldType();
        $merchantDefinedField1->setName("customerLoyaltyNum");
        $merchantDefinedField1->setValue("1128836273");

        $merchantDefinedField2 = new AnetAPI\UserFieldType();
        $merchantDefinedField2->setName("favoriteColor");
        $merchantDefinedField2->setValue("blue");

        // Create a TransactionRequestType object and add the previous objects to it
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($input['amount']);
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setBillTo($customerAddress);
        $transactionRequestType->setCustomer($customerData);
        $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
        $transactionRequestType->addToUserFields($merchantDefinedField1);
        $transactionRequestType->addToUserFields($merchantDefinedField2);

        // Assemble the complete transaction request
        $requests = new AnetAPI\CreateTransactionRequest();
        $requests->setMerchantAuthentication($merchantAuthentication);
        $requests->setRefId($refId);
        $requests->setTransactionRequest($transactionRequestType);

        // Create the controller and get the response
        $controller = new AnetController\CreateTransactionController($requests);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if ($response != null) {
            $product=json_decode($input['product']);
            $product_id=[];
            $qty=0;
            foreach($product as $val){
                $product_id[]=$val->product_id;
                $qty=$qty+$val->qty;
            };
            $product_str=implode(',',$product_id);
            // Check to see if the API request was successfully received and acted upon
            if ($response->getMessages()->getResultCode() == "Ok") {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $message_text = $tresponse->getMessages()[0]->getDescription().", Transaction ID: " . $tresponse->getTransId();
                    $msg_type = "success_msg";    
                    $res=new PaymentLogs();
                    $res->amount = $input['amount'];
                    $res->response_code = $tresponse->getResponseCode();
                    $res->transaction_id = $tresponse->getTransId();
                    $res->auth_id = $tresponse->getAuthCode();
                    $res->message_code = $tresponse->getMessages()[0]->getCode();
                    $res->name_on_card = trim($input['card_holder_name']);
                    $res->quantity=$qty;
                    $res->product_id=$product_str;
                    $res->billing_address_id=$result;
                    if($request->code){
                        $code=DB::table('coupons')->where('code',$request->code)->first();
                        $res->coupon_code_id=$code->id;
                    }
                    $res->save();

                    foreach($product as $val){
                        $buy_product=[
                            'product_id'=>$val->product_id,
                            'qty'=>$val->qty,
                            'price'=>get_product_by_id($val->product_id)->price,
                            'payment_log_id'=>$res->id,
                        ];
                        DB::table('buy_product')->insert($buy_product);
                    };

                    //After payment send mail of user and admin start 
                    $details = [
                        'billing_details' => $request->all(),
                        'product'=>$product,
                        'payble_amount'=>$input['amount'],
                        'invoice_no'=>$refId
                    ];
                    if($request->code){
                        $details['code'] = true;
                    }
                    Mail::to([$request->email,'kumaraojha@gmail.com'])->send(new Invoice($details));
                    //After payment send mail of user and admin End
                    return redirect()->route('thank-you');
                    
                } else {
                    $message_text = 'There were some issue with the payment. Please try again later.';
                    $msg_type = "error_msg";                                    

                    if ($tresponse->getErrors() != null) {
                        $message_text = $tresponse->getErrors()[0]->getErrorText();
                        $msg_type = "error_msg";                                    
                    }
                }
                // Or, print errors if the API request wasn't successful
            } else {
                $message_text = 'There were some issue with the payment. Please try again later.';
                $msg_type = "error_msg";                                    

                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $message_text = $tresponse->getErrors()[0]->getErrorText();
                    $msg_type = "error_msg";                    
                } else {
                    $message_text = $response->getMessages()->getMessage()[0]->getText();
                    $msg_type = "error_msg";
                }                
            }
        } else {
            $message_text = "No response returned";
            $msg_type = "error_msg";
        }
        return back()->with($msg_type, $message_text)->withInput();
    }
}
