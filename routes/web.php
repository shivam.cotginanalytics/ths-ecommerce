<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/{slug?}',[FrontController::class,'index'])->name('index');
Route::get('product/{product_slug}',[FrontController::class,'product'])->name('product-detais');
Route::get('cart',[FrontController::class,'cart']);

Route::get('product-detail/', function () {
    return view('product-detail');
});
Route::get('payment/success', function () {
    return view('thankyou');
})->name('thank-you');

Route::post('add_to_cart',[FrontController::class,'add_to_cart'])->name('add-to-cart');
Route::post('product/quantity',[FrontController::class,'product_quantity'])->name('product-quantity');

// Route::get('billing/page', function () {
//     return view('billing-page');
// })->name('billing-page');

Route::get('cart/view', function () {
    return view('view-cart');
})->name('view-cart');
Route::get('user/invoice', function () {
    return view('email.invoice');
})->name('invoice');

Route::match(['get','post'],'billing/page/', [FrontController::class,'biling_page'])->name('billing-page');

Route::match(['get','post'],'make/payment/', [PaymentController::class,'handleonlinepay'])->name('make-payment');
Route::match(['get','post'],'store/enquire', [PaymentController::class,'store_enquire'])->name('store-enquire');
Route::match(['get','post'],'apply/coupon/', [FrontController::class,'check_coupon'])->name('apply-coupon');

Route::post('contact-request', [HomeController::class,'add_request'])->name('request');
Route::post('subscribe', [HomeController::class,'subscribe'])->name('subscribe');