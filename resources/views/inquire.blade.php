@include('layout/head')
<style>
    .aboutUs {
        margin: 10px;
        background-color: var(--red);
    }
    
    .product-bill-details {
        padding: 2.5rem;
        
        
        border-radius: 2px;
        background-color: white;
    }
    
    .btns-billing {
        text-align: center;
        
    }
    
    .about-heading {
        color: var(--white);
        font-weight: 700;
        font-size: 40px;
        line-height: 47px;
        display: flex;
        justify-content: center;
        position: relative;
        text-align: center;
    }
    
    .bill-input {
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
        border-radius: 0px;
        background-color: #FAFAFA;
    }
    
    .prod-img {
        width: 30%;
        height:
        19%
    }
    .bill-select{
        border-top: 0px;
        border-right: 0px;
        border-left: 0px;
        border-radius: 0px;
        background-color:#FAFAFA;
    }
    
    .text-zone ::placeholder{
        
    }
    
    .coupan-code {
        border: 1px solid #DEDEDE;
        border-radius: 2px;
        width: 50%
    }
    
    .apply-btn {
        width: 30%;
        border: none;
        background-color: var(--red);
        color: white;
        border-radius: 2px
    }
    
    
    
    .make-my-payment {
        background: var(--red);
        color: white;
        border: none;
        border-radius: 2px
    }
    
    .payment-btn {
        text-align: center;
        
    }
    
    .text-zone {
        border: 0.5px solid #D9D9D9;
        border-radius: 4px;
        width: 100%;
        color: #1e1e1e;
        padding: 15px
    }
    .billing-container{
        background: #FAFAFA;
        padding: 2.5rem 1.5rem 2.5rem 1.5rem
    }
    .billing-container .form-select:focus {
        border-color:var(--red);
        outline: 0;
        box-shadow: none;
    }
    .billing-container textarea:focus-visible{
        border-color: var(--red);
        outline: 0;
    }
    .product-bill-details td a {
        font-weight: 500;
        font-size: 16px;
        line-height: 19px;
        color: #333333;
    }
    .product-bill-details .table th {
        font-weight: 600;
    }
    .billing input[type="checkbox"] {
        appearance: none;
        width: 15px;
        height: 15px;
        border: 2px solid var(--blue);
        position: relative;
    }
    .billing input[type="checkbox"]:checked::before {
        position: absolute;
        content: "";
        background: url(assets/images/check.svg) no-repeat;
        top: 50%;
        width: 90%;
        height: 90%;
        background-position: center;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    @media(max-width:767px) {
        .coupan-code {
            font-size: 10px
        }

        .butn{
            padding: 5px 18px;
        }
        
        .apply-btn {
            font-size: 10px;
            margin: auto;
            
        }
        .call-button{
            margin: auto
        }
        
        .payment-btn {
            font-size: 10px;
           margin-bottom: 20px;
        }
.btns-billing{
    display: flex;
    flex-direction: column;
    gap: 15px
}

.btns-billing input{
    margin: auto;
    width: 50%;
}

.table tr{
    display: flex;
    flex-direction: column;
   position: relative;
}

.table tr td svg{
    position: absolute;
        top: -25px;
    right: 0px;
}

.table tr td{
    border: none
}

.product-bill-details {
    padding: 0.5rem;
    border: 1px solid #DEDEDE;
    border-radius: 2px;
}

    }
</style>

@include('layout/header')
<section class="aboutUs m-0 p-10">
    <h1 class="about-heading m-0 p-0">BILLING DETAILS</h1>
</section>

<div class="container py-3">
    <nav style="--bs-breadcrumb-divider: '>>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ URL::To('/') }}">
                    <svg width="23" height="21" viewBox="0 0 23 21" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                    d="M1 8.94369L10.0675 1.51755C10.9101 0.827482 12.0899 0.827482 12.9325 1.51755L22 8.94369M4.5 6.07724V17.543C4.5 18.9 5.54467 20 6.83333 20H8H15H16.1667C17.4553 20 18.5 18.9 18.5 17.543V1.57281M9.16667 20V12.6291C9.16667 11.9507 9.689 11.4006 10.3333 11.4006H12.6667C13.311 11.4006 13.8333 11.9507 13.8333 12.6291V20"
                    stroke="#B31942" stroke-width="1.5" stroke-linecap="round" />
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item active" aria-current="page"><span class="red-color">Solutions</span> </li>
        <li class="breadcrumb-item active" aria-current="page"><span class="blue-color">Billing Details</span></li>
    </ol>
</nav>
</div>

<section class="billing">
    <div class="container billing-container ">
        <div class="row">
            <div class="col-lg-7">
                <h2><span class="blue-color">Billing Details</span></h2>
                <form>
                    <label class="mt-4 mb-1">First Name<span class="red-color">*:</span> </label>
                    <input class=" bill-input form-control">
                    <label class="mt-4 mb-1">Last Name <span class="red-color">:</span> </label>
                    <input class="bill-input form-control">
                    <label class="mt-4 mb-1">Company / Region<span class="red-color">*:</span> </label>
                    <input class=" bill-input form-control" placeholder="India">
                    <label class="mt-4 mb-1">Street address<span class="red-color">*:</span> </label>
                    <input class=" bill-input form-control" placeholder="House number and street name">
                    <br>
                    <input class="bill-input form-control"
                    placeholder="Apartment, Building, Area, Unit, etc. (optional)">
                    <label class="mt-4 mb-1">Town/City<span class="red-color">*:</span></label>
                    <input class=" bill-input form-control">
                    <label class="mt-4 mb-1">State<span class="red-color">*:</span></label>
                    <input class=" bill-input form-control">
                    <label class="mt-4 mb-1">Pin Code<span class="red-color">*:</span></label>
                    <input class=" bill-input form-control" placeholder=" ">
                    <label class="mt-4 mb-1">Email address<span class="red-color">*:</span></label>
                    <input class="bill-input form-control" placeholder=" ">
                    <label class="mt-4 mb-1">Company name: (Optional)</label>
                    <input class="bill-input form-control" placeholder=" ">
                    <label class="mt-4 mb-3">Order note: (Optional)</label>
                    <textarea placeholder="Notes about your order, e.g. special notes for delivery" class="text-zone" name="" id="" cols="" rows="7"></textarea>
                    <div class="form-check mt-4 mb-1 d-flex">
                        <span><input class="me-2" type="checkbox" value="" id="flexCheckDefault"></span>
                        <label class="form-check-label" for="flexCheckDefault">
                            I have read and agree to Trust haven solution 
                            <a href="#" class="red-color">Terms of service</a>
                            and <a href="#" class="red-color">Privacy Policy</a>.
                        </label>
                    </div>
                    
                    
                </form>
            </div>
            <div class="col-lg-5">
                <div class="product-bill-details">
                    <h3><span class="red-color">Products</span></h3>
                    <table class="table">
                        
                        <tbody>
                            <tr class="product">
                                <th scope="row"><a href="#"><img src="./assets/images/product-listing/product-1.jpg" alt=""></a></th>
                                <td><a href="#">HP 15s-dr3506TU Intel Core i3 11th Gen
                                    (15.6 inch,8GB, 1TB and 256GB)..</a>
                                    <p><span>QTY     </span>: 2</p>
                                </td>
                                <td>$180.00</td>
                                <td class="close"><span class="red-color "><i class="fa-solid fa-circle-xmark"></span></i></td>
                            </tr>
                            <tr class="product">
                                <th scope="row"><a href="#"><img src="./assets/images/product-listing/product-1.jpg" alt=""></a></th>
                                <td><a href="#">HP 15s-dr3506TU Intel Core i3 11th Gen
                                    (15.6 inch,8GB, 1TB and 256GB)..</a>
                                    <p><span>QTY     </span>: 2</p>
                                </td>
                                <td>$180.00</td>
                                <td class="close"><span class="red-color"><i class="fa-solid fa-circle-xmark"></span></i></td>
                            </tr>
                            <tr class="product">
                                <th scope="row"><a href="#"><img src="./assets/images/product-listing/product-1.jpg" alt=""></a></th>
                                <td><a href="#">HP 15s-dr3506TU Intel Core i3 11th Gen
                                    (15.6 inch,8GB, 1TB and 256GB)..</a>
                                    <p><span>QTY     </span>: 2</p>
                                </td>
                                <td>$180.00</td>
                                <td class="close"><span class="red-color"><i class="fa-solid fa-circle-xmark"></span></i></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table">
                        
                        <tbody>
                            <tr>
                                <th scope="row">Sub Total</th>
                                
                                <td class="text-end" colspan="3">$180.00</td>
                            </tr>
                            <tr>
                                <th scope="row">Tax</th>
                                
                                <td class="text-end" colspan="3">$ 89.00</td>
                            </tr>
                            <tr>
                                <th scope="row">Discount Coupon</th>
                                
                                <td class="text-end" colspan="3">$ 5.00</td>
                            </tr>
                            <tr>
                                <th scope="row">Total</th>
                                
                                <td class="text-end" colspan="3">$6,568.00</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="btns-billing pt-3">
                        <input type="text" class="coupan-code p-2" placeholder="Coupon Code">
                        <a class="call-button butn" href="#">Apply</a>
                        
                    </div>
                    <div class="payment-btn pt-4 mt-4">
                        
                        <a class="call-button butn " href="#">Make Payment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('layout/footer');
<script>
    $('.close').click(function(){
        $(this).parent('.product').remove();
    });
</script>
