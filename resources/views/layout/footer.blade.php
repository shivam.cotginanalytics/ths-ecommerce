{{-- <div class="modal-form">
    @include('layout.book-appointment')

</div> --}}
{{-- <div class="container subscribe">
    <div class="join-us">
        <div class="row justify-content-center">
            <div class="col-xxl-3 col-lg-4 col-md-12">
                <h3 class="join mb-0">SUBSCRIBE -</h3>
                <p class="mb-lg-0 mb-3">To receive updates & offers in your inbox.</p>
            </div>
            <div class="col-xxl-5 col-lg-6 col-md-12 subcribe-mail-box">
                @if(Session::get('subscribe_message'))
                <p class="text-success">{{ Session::get('subscribe_message') }}</p>
                @endif
                <form action="{{ route('subscribe') }}" method="post">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <div class="mb-lg-0 mb-3 d-flex">
                            <span class="input-group-text">Email</span>
                            <input type="text" class="form-control" placeholder="Enter Email..."
                                aria-label="Enter Email..." name="email" />
                        </div>
                        <button class="btn butn ms-3" type="button">Submit
                            <svg width="18" height="16" viewBox="0 0 18 16" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M4.56092 7.70147H8.56092M4.29112 7.2698L1.13074 2.21337C0.653254 1.44943 1.57271 0.59586 2.45211 0.986688L15.7721 6.90645C16.5091 7.23402 16.5091 8.16892 15.7721 8.49648L2.4521 14.4162C1.57271 14.8071 0.653253 13.9535 1.13073 13.1896L4.29112 8.13313C4.45891 7.86468 4.45891 7.53825 4.29112 7.2698Z"
                                    stroke="white" stroke-width="1.5" stroke-linecap="round" />
                            </svg>
                        </button>
                    </div>
                    @error('email')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </form>
            </div>
        </div>
    </div>
</div> --}}

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div>
                    <a href=""><img class="footer-logo" src="{{ asset('assets/images/logo.svg') }}" /></a>
                    <p class='address footer-head mb-1'>Corporate Address :</p>
                    <p> <span class="blue-color">6915 Jennie Anne Ct, Bakersfield, CA, 93313</span> </p>

                    <p class="mail-id footer-head">
                        <span class='red-color'>Email : </span>
                        <a href="mailto:help@trusthavensolution.com" class="blue-color">help@trusthavensolution.com</a>
                    </p>
                    <p class="footer-head">
                        <span class='red-color'>Toll-Free : </span>
                        <a href="tel:1800-235-0122" class="blue-color">1800-235-0122</a>
                    </p>
                </div>
                <div class="follow-us footer-head">
                    <p class="follow"><span>Follow Us :</span></p>
                    <a href=" https://www.facebook.com/trustheavensolution/" target="_blank">
                        <i class="fa-brands fa-facebook"></i>
                    </a>
                    <a href="https://www.instagram.com/trusthavensolution/" target="_blank">
                        <i class="fa-brands fa-instagram"></i></i>
                    </a>
                    <a href="https://twitter.com/trust_haven1" target="_blank">
                        <i class="fa-brands fa-twitter"></i>
                    </a>
                    <a href="https://www.linkedin.com/company/92499923/admin/" target="_blank">
                        <i class="fa-brands fa-linkedin"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 ">
                <p class="info">INFO</p>
                <ul class="list-details">
                    <li>
                        <a class='blue-color' href="">About Us</a>
                    </li>
                    <li>
                        <a class='blue-color' href="">Contact Us</a>
                    </li>
                    <li>
                        <a class='blue-color' href="">Our Blog</a>
                    </li>
                    <li>
                        <a class='blue-color book-appointment-btn' href="javascript:voild(0)">Book Appointment</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6">
                <p class="info">BUY NEW DEVICES</p>
                <ul class="list-details">
                    @if(get_catrgory())
                    @foreach(get_catrgory() as $list1)
                    <li>
                        <a class='blue-color' href="{{ url('/',$list1->category_slug) }}">{{ $list1->category_name }}</a>
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
            <div class="col-lg-2 col-md-6">
                <p class="info">QUICK LINKS</p>
                <ul class="list-details">
                    <li><a class='blue-color' href="">Privacy Policy</a></li>
                    <li><a class='blue-color' href="">Terms & Condition</a></li>
                    <li><a class='blue-color' href="">Refund Policy</a></li>
                    <li><a class='blue-color' href="">FAQ</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="disclaimer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="mb-0"><b>Disclaimer :</b> Trust Haven Solution is a provider of IT support and services
                        that are independently owned and managed. We are associated with major brands or service
                        providers like Microsoft, HP, Dell, Lenovo, CISCO, Carbonite, Webroot, Malwarebytes, etc. Our
                        software and services may be is to resolve difficulties with your computer, laptop, or phone.
                        All other trademarks are owned by their respective proprietors.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <p class="p-0 m-0">© All rights reserved - <a href="">Trust Haven Solution Inc.</a></p>
    </div>
</footer>

{{-- Whene cross the price limit show modal --}}
<div class="modal fade" id="price_limit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Price range</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            You are not able to buy in the $3000+ price range.
            <a href="{{ route('billing-page') }}?type=1">Click here</a> to enquire now.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
      </div>
    </div>
  </div>
  {{-- Whene cross the price limit show modal end --}}
<input type="hidden" value="{{ config('app.trust_haven_app_url') }}" id="app_url">
<input type="hidden" value="{{ route('add-to-cart') }}" id="add_to_cart_url">
<input type="hidden" value="{{ route('product-quantity') }}" id="add_p_quentity_url">

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}" async defer></script>
<script src="{{ asset('js/slick.min.js') }}"></script>
<script src="{{ asset('js/jquery.ez.min.js') }}"></script>
<script src="{{ asset('js/all.min.js') }}" async defer></script>
<script src="{{ asset('js/custom.js') }}?v=<?= time() ?>"></script>

<!--Start of Tawk.to Script-->
{{-- <script type="text/javascript" async defer>
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/61bb1781c82c976b71c1bb67/1fn1d7lng';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script> --}}
<!--End of Tawk.to Script-->

<script>
    $("#modal_submit").click(function(event) {
        event.preventDefault();
        // Get form
        var form = $('#Let_Talk')[0];
        var data = new FormData(form);
        data.append("CustomField", "This is some extra data, testing");

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('request') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function(data) {
                console.log(data);
                if (data.status === true) {
                    location.reload(true);
                    alert(data.message);
                }
            },
            error: function(e) {
                // console.log(e.responseJSON.errors.g-recaptcha-response);
                if (e.responseJSON.errors.name) {
                    $('#name_error').html(e.responseJSON.errors.name);
                }
                if (e.responseJSON.errors.email) {
                    $('#email_error').html(e.responseJSON.errors.email);
                }
                if (e.responseJSON.errors.number) {
                    $('#number_error').html(e.responseJSON.errors.number);
                }
                $.each(e.responseJSON.errors, function(key, value) {
                    if (key == 'g-recaptcha-response') {
                        $('#captcha_error').html(value);
                    }
                })
            }
        });

    });
</script>

<!-- BEGIN: BBB AB Seal -->
<script type="text/javascript">
	var bbb = bbb || [];
	bbb.push(["bbbid", "central-california-inland-empire"]);
	bbb.push(["bid", "850075443"]);
	bbb.push(["chk", "2AFE8928D4"]);
	bbb.push(["pos", "bottom-left"]);
	(function () {
	    var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
	    var bbb = document.createElement("script");
	    bbb.type = "text/javascript";
	    bbb.async = true;
	    bbb.src = scheme + "seal-cencal.bbb.org/badge/badge.min.js";
	    var s = document.getElementsByTagName("script")[0];
	    s.parentNode.insertBefore(bbb, s);
	})();
</script>

<script>
//For add to cart
function add_to_cart(id,del='') {
    let trust_haven_url=$('#app_url').val();
    let url=$('#add_to_cart_url').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = new FormData();
    data.append('id', id);
    data.append('delete', del);
    $('#loader-icon' + id).removeClass('d-none');
    $('#shop-icon' + id).addClass('d-none');
    $.ajax({
        url: "{{ route('add-to-cart') }}",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function(response) {
            let html='';
            let total_price=0;
            // Handle success response
            if(del){
             location.reload(true);
            };
            $.each(response.product,function(key,val){
                if(val.price){
                total_price=total_price+val.price;
            }
            var img_url=trust_haven_url+'storage/images/'+val.image;
            html +='<div class="add-view-cart d-flex"><div class="cart_product_image">';
            html +='<a href="javascript:void(0)" class="product-images">';
            html +='<img src="'+img_url+'" style="width:40px;" alt="img"></a></div>';
            html +='<div class="cart-product-title">';
            html +='<h5><a href="javascript:void(0)">'+val.name+'</a></h5>';
            html +='<p class="price_and_qu">Price:- '+val.price+'</p></div>';
            html +='<div class="btn-remove" role="button" onclick="DeleteCartProduct('+val.id+')"><img src="{{asset('assets/images/remove-1.svg')}}" alt="Close" title="Close" /></div></div>';
            });
            html +='<div class="subtotal-price"><ul>';
            html +='<li>Subtotal : <span class="red-color"> $'+total_price+'</span></li></ul></div>';
            html +='<div class="btn-box">';
            html +='<a class="contact-button butn me-3" href="{{ route('view-cart') }}">View Cart</a>';
            html +='<a class="call-button butn" href="#">Buy Now</a></div>';
            $('#cart_data').html(html);


            $('.add_to_cart_count').html(response.product_count);
            $('#loader-icon' + id).addClass('d-none');
            $('#shop-icon' + id).removeClass('d-none');
            // $('html, body').animate({
            //     scrollTop: top
            // }, 100);
        },
        error: function(xhr, textStatus, errorThrown) {
            // Handle error response
        }
    });
}
</script>
<!-- END: BBB AB Seal -->






