<style>
    .counter{
        text-align: center;
        top: -6px;
        background-color: #ffd6dd;
        color: #b31942;
        border-radius: 50%;
        left: 32px;
        height: 25px;
        width: 25px;
        line-height: 1.6
    }
    .btn-remove {
        position: absolute;
        right: 0;
    }
    .cart-product-title {
    margin-right: 1.8rem;
    }
</style>


<header class="header">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="{{URL::To('/')}}">
                <img src="{{asset('assets/images/logo.svg')}}" alt="Logo" />
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                <div class="offcanvas-header">
                    <a class="navbar-brand" href="{{URL::To('/')}}">
                        <img class="header-inner" src="{{asset('assets/images/logo.svg')}}" alt="Logo" />
                    </a>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <div class="navbar-collapse">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">About</a>
                            </li>
                            {{-- @php
                            $route=['quick-books','google-ads-services','search-engine-optimization','social-media-marketing','website-development','website-designing','microsoft-server','laptop-security','adware-removal','spyware-removal','antivirusProtection','online-virus-removal','microsoft-office','h-and-r-block','firewall','computer-support','remote-support','network-support','printer-support','laptop-support','window-upgrade','email-support','outlook-support','windows-support','computer-security'];
                            @endphp --}}
                            
                            <li class="nav-item dropdown-mb">
                                <a class="nav-link" href="#">Blog</a>
                            </li>
                            <li class="nav-item dropdown-mb">
                                <a class="nav-link" href="">Contact</a>

                            </li> 
                            {{-- <li class="nav-item dropdown-mb">
                                <a class="nav-link book-appointment-btn" href="javascript:void(0)">Book Appointment</a>

                            </li> --}}
                            
                        </ul>
                        <div class="d-flex">
                            {{-- <a href="#" class="nav-link last-button">
                                <svg width="23" height="24" viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2.98304 10.0004H1M3.97456 13.0004H1.99152M2.34962 18.81C6.07856 23.5811 12.9364 24.4001 17.6671 20.6393C22.3977 16.8785 23.2097 9.96198 19.4808 5.19087C15.7519 0.419774 8.89402 -0.399214 4.16337 3.36161M4.96609 7.00044H5.34555C5.98431 7.00044 6.5206 7.48076 6.58988 8.11488L7.09742 12.761C7.16669 13.3951 7.70298 13.8754 8.34174 13.8754H13.7568C14.329 13.8754 14.8283 13.488 14.9697 12.9343L15.8334 9.55239C16.0352 8.76203 15.4372 7.9935 14.6205 7.9935H7.21916M7.23465 16.141H7.70404M7.23465 16.6098H7.70404M14.119 16.141H14.5884M14.119 16.6098H14.5884M8.09535 16.3754C8.09535 16.7206 7.81515 17.0004 7.4695 17.0004C7.12385 17.0004 6.84365 16.7206 6.84365 16.3754C6.84365 16.0303 7.12385 15.7504 7.4695 15.7504C7.81515 15.7504 8.09535 16.0303 8.09535 16.3754ZM14.9797 16.3754C14.9797 16.7206 14.6995 17.0004 14.3539 17.0004C14.0082 17.0004 13.728 16.7206 13.728 16.3754C13.728 16.0303 14.0082 15.7504 14.3539 15.7504C14.6995 15.7504 14.9797 16.0303 14.9797 16.3754Z" stroke="white" stroke-width="1.5" stroke-linecap="round" />
                                </svg>
                                BUY DEVICE
                            </a> --}}
                            <a href="#" class="nav-link last-button rounded-circle">
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </a>
                            <a href="#"  type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"class="nav-link last-button position-relative cart-icon rounded-circle">
                                @php
                                //   $getAddToCartTotalItem = getAddToCartTotalItem();
                                  $totalCountItem = count(get_user_card());
                                @endphp
                                <span class="position-absolute counter add_to_cart_count">{{$totalCountItem}}</span><i class="fa-solid fa-cart-shopping"></i>
                            </a>
                            
                            {{-- <a href="#" class="nav-link last-button rounded-circle">
                            <i class="fa-solid fa-user"></i>
                            </a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

<div class="add-cart-box offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 class="offcanvas-title" id="offcanvasRightLabel">Cart</h5>
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="aa-cartbox-summary" id="cart_data">
        @if(count(get_user_card()) > 0)
        @php $total_price=0; @endphp
        @foreach(get_user_card() as $cartItem)
        @php
            $cart_data=get_cart($cartItem->product_id);
            if($cart_data->qty_price){
                $total_price=$total_price+$cart_data->qty_price;
            }else{ 
                $total_price=$total_price+$cartItem->product_price;
            }
        @endphp
        <div class="add-view-cart d-flex position-relative">
            <div class="cart_product_image">
                <a href="javascript:void(0)" class="product-images">
                    <img src="{{config('app.trust_haven_app_url').('storage/images/'.get_product_by_id($cartItem->product_id)->image)}}" style="width:40px;" alt="img">
                </a>
            </div>
            <div class="cart-product-title">
                <h5><a href="javascript:void(0)">{{get_product_by_id($cartItem->product_id)->name}}</a></h5>
                {{-- <p class="price_and_qu"><span>{{5}} x </span> {{$cartItem->price}}</p> --}}
                <p class="price_and_qu">Price:- {{get_product_by_id($cartItem->product_id)->price}}</p>
            </div>
            <div class="btn-remove" role="button" onclick="DeleteCartProduct({{$cartItem->id}})">
                <img src="{{asset('assets/images/remove-1.svg')}}" alt="Close" title="Close" />
            </div>
        </div>
        @endforeach  
        <div class="subtotal-price">
            <ul>
                <li>Subtotal : <span class="red-color"> $ {{ number_format($total_price)}}</span></li>
            </ul>
        </div>
        <div class="btn-box">
            <a class="contact-button butn me-3" href="{{ route('view-cart') }}">View Cart</a>
            <a class="call-button butn" href="{{ route('billing-page') }}">Buy Now</a>
        </div>

        @else
        <div class="card text-center">
            <div class="card-header empty_product">
              Empty
            </div>
            <div class="card-body">
              <h5 class="card-title">Your cart is Empty!</h5>
            </div>
            <div class="card-footer text-muted">
              {{-- 2 days ago --}}
            </div>
          </div>
        @endif
    </div>
  </div>
</div>