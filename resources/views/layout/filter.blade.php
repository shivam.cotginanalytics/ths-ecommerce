<div class="position-sticky top-20">
    <div class="input-group mb-3">
        <input type="text" class="search-input form-control p-2"
            placeholder="Search your product..." aria-label="Recipient's username"
            aria-describedby="button-addon2">
        <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i
                class="fa-solid red-color fa-magnifying-glass"></i></button>
    </div>
    <div class="filters-heading text-danger border-bottom border-danger">
        <h4 class="">Filters <i class="fa-solid fa-chevron-down float-end d-lg-none"></i>
        </h4>
    </div>
    <div class=" accordion accordion-display" id="accordionPanelsStayOpenExample">

        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header  text-danger border-bottom border-danger">
                <button class="accordion-button accordian-listing-btn" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                    aria-controls="collapseTwo">
                    BRAND
                </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse show" data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">
                    <input class="blue-color" type="radio" id="check1" name="check"
                        value="">
                    <label class="blue-color" id="check1" for="check1"> Under
                        <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check2" name="check"
                        value="">
                    <label class="blue-color" for="check2">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check3" name="check"
                        value="">
                    <label class="blue-color" for="check3">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check4" name="check"
                        value="">
                    <label class="blue-color" for="check4">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>
                </div>
            </div>
        </div>
        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header  text-danger border-bottom border-danger">
                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseThree"
                    aria-expanded="false" aria-controls="collapseThree">
                    PRICE
                </button>
            </h2>
            <div id="collapseThree" class="accordion-collapse collapse"
                data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">

                    <input class="blue-color" type="radio" id="check5" name="check"
                        value="">
                    <label class="blue-color" for="check5"> Under <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check6" name="check"
                        value="">
                    <label class="blue-color" for="check6">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check7" name="check"
                        value="">
                    <label class="blue-color" for="check7">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check8" name="check"
                        value="">
                    <label class="blue-color" for="check8">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>


                </div>
            </div>
        </div>
        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header  text-danger border-bottom border-danger">
                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true"
                    aria-controls="collapseFour">
                    DISPLAY SIZE
                </button>
            </h2>
            <div id="collapseFour" class="accordion-collapse collapse "
                data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">
                    <input class="blue-color" type="radio" id="check9" name="check"
                        value="">
                    <label class="blue-color" for="check9"> Under <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check10" name="check"
                        value="">
                    <label class="blue-color" for="check10">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check11" name="check"
                        value="">
                    <label class="blue-color" for="check11">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check12" name="check"
                        value="">
                    <label class="blue-color" for="check12">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>
                </div>
            </div>
        </div>
        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header  text-danger border-bottom border-danger">
                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="true"
                    aria-controls="collapseFive">
                    STORAGE
                </button>
            </h2>
            <div id="collapseFive" class="accordion-collapse collapse "
                data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">
                    <input class="blue-color" type="radio" id="check13" name="check"
                        value="">
                    <label class="blue-color" for="check13"> Under <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check14" name="check"
                        value="">
                    <label class="blue-color" for="check14">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check15" name="check"
                        value="">
                    <label class="blue-color" for="check15">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check16" name="check"
                        value="">
                    <label class="blue-color" for="check16">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>
                </div>
            </div>
        </div>
        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header  text-danger border-bottom border-danger">
                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="true"
                    aria-controls="collapseSix">
                    PROCESSOR
                </button>
            </h2>
            <div id="collapseSix" class="accordion-collapse collapse "
                data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">
                    <input class="blue-color" type="radio" id="check17" name="check"
                        value="">
                    <label class="blue-color" for="check17"> Under <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check18" name="check"
                        value="">
                    <label class="blue-color" for="check18">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check19" name="check"
                        value="">
                    <label class="blue-color" for="check19">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check20" name="check"
                        value="">
                    <label class="blue-color" for="check20">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>
                </div>
            </div>
        </div>
        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header  text-danger border-bottom border-danger">
                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseSeven"
                    aria-expanded="true" aria-controls="collapseSeven">
                    RAM SIZE
                </button>
            </h2>
            <div id="collapseSeven" class="accordion-collapse collapse "
                data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">
                    <input class="blue-color" type="radio" id="check21" name="check"
                        value="">
                    <label class="blue-color" for="check21"> Under <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check23" name="check"
                        value="">
                    <label class="blue-color" for="check23">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check24" name="check"
                        value="">
                    <label class="blue-color" for="check24">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check25" name="check"
                        value="">
                    <label class="blue-color" for="check25">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>
                </div>
            </div>
        </div>
        <div class="accordion-item accordion-listing-item">
            <h2 class="accordion-header">
                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                    data-bs-toggle="collapse" data-bs-target="#collapseEight"
                    aria-expanded="true" aria-controls="collapseEight">
                    DISCOUNT
                </button>
            </h2>
            <div id="collapseEight" class="accordion-collapse collapse "
                data-bs-parent="#accordionExample">
                <div class="accordion-body checked-div">
                    <input class="blue-color" type="radio" id="check26" name="check"
                        value="">
                    <label class="blue-color" for="check26"> Under <span>&nbsp</span>
                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                    <input class="blue-color" type="radio" id="check27" name="check"
                        value="">
                    <label class="blue-color" for="check27">$500 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                    <input class="blue-color" type="radio" id="check28" name="check"
                        value="">
                    <label class="blue-color" for="check28">$600 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                    <input class="blue-color" type="radio" id="check29" name="check"
                        value="">
                    <label class="blue-color" for="check29">$750 <span>&nbsp</span>
                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                        $900</label><br><br>
                </div>
            </div>
        </div>
    </div>
</div>