@include('layout/head')
<style>
    .accordion-button {
        padding: 0;
    }

    .products-category p {
        color: var(--blue);
        margin-top: 5px;
    }

    .btn-outline-secondary {
        border-left: 0;
        border-right: 1px solid #B31942;
        border-bottom: 1px solid #B31942;
        border-top: 1px solid #B31942;
        border-radius: 4px;

    }

    .search-input {
        border-left: 1px solid #B31942;
        border-top: 1px solid #B31942;
        border-bottom: 1px solid #B31942;
        border-right: 0;
        border-radius: 4px
    }

    .btn-outline-secondary:hover {
        background-color: unset;
        border-color: #B31942
    }

    .btn-outline-secondary:active {
        background: unset
    }

    #button-addon2:active {
        background: unset
    }

    .checked-div{
        background-color: #fffafb;
        border-radius: 0 0 4px 4px;
    }
    .checked-div input[type="radio"] {
        appearance: none;
        width: 15px;
        height: 15px;
        border: 2px solid var(--blue);
        position: relative;
        margin-top: 0.8rem;
        position: relative
    }

    .checked-div input[type="radio"]:checked::before {
        position: absolute;
        content: "";
        background: url(assets/images/check.svg) no-repeat;
        top: 50%;
        width: 90%;
        height: 90%;
        background-position: center;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .products-category .row {
        padding: 1.5rem;
        border-radius: 10px;
    }

    @media(max-width:991px) {
        .accordion-display {
            display: none
        }
    }

    .active_category{
        box-shadow: 0px 2px 7px 0px #dddddd;
    }
</style>


@include('layout/header')
{{-- @php dd(config('app.trust_haven_app_url')); @endphp --}}
<section class="product-listing-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-6">
                <h1 class="banner-head">Explore Your Needs</h1>
                <p>Laptops are a quick and easy way to store information from the web. They can be used for gaming,
                    entertainment, travel and business, processing and multitasking, and much more.</p>
            </div>
            <div class="col-lg-4 offset-lg-1 col-md-6">
                <div class="product-listing-slider">
                    {{-- @foreach($home_banners as $list)  --}}
                    <figure class="mb-0">
                        <img src="{{config('app.trust_haven_app_url').('storage/images/'.$home_banners[0]->image)}}" alt="{{$home_banners[0]->title}}"/>
                    </figure>
                    {{-- @endforeach --}}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="products-category ">
    <div class="container">
        <div class="row bg-light">
            @foreach(get_catrgory() as $list1)
            <div class="col-lg-3 col-6 text-center mb-lg-0 mb-3">
                <a href="{{ url('/',$list1->category_slug) }}">
                    <button class="bg-transparent border-0">
                        <img src="{{config('app.trust_haven_app_url').('storage/images/'.$list1->category_image)}}" alt="{{$list->category_name}}" class="@php if($list1->category_slug == collect(request()->segments())->last()) echo 'active_category'; @endphp"/>
                        <p class="mb-0 blue-color">{{$list1->category_name}}</p>
                    </button>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>


<section class="filter-products">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                @include('layout.filter')
            </div>
            <div class="col-lg-9">
                <div class="row row-gap-4 mb-4">
                    @foreach($home_products as $list)
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="product-list">
                            <figure class="position-relative">
                                <a href="{{url('product/'.$list->slug)}}">
                                    <img src="{{config('app.trust_haven_app_url').('storage/images/'.$list->image)}}" alt="{{$list->name}}" />
                                </a>
                            </figure>
                            <div class="prod-name mb-2" title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                                <a href="{{url('product/'.$list->slug)}}" class="blue-color">{{$list->name}}</a>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <div>
                                    <div class="price d-inline-block me-1">$ {{$list->price}}</div>
                                    <div class="stock red-color d-inline-block">In stock</div>
                                </div>
                                <div>
                                    <div class="rate-text d-inline-block me-1">4.5</div>
                                    <div class="rating d-inline-block">
                                        <a href="javascript:void(0)">
                                            <i class="fa-solid fa-star"></i>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <i class="fa-solid fa-star"></i>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <i class="fa-solid fa-star"></i>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <i class="fa-solid fa-star"></i>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <i class="fa-regular fa-star"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <a href="javascript:void(0)" onclick="add_to_cart({{ $list->id }})" class="btn butn me-2 text-uppercase add bg-blue">Add to cart
                                    <div class="spinner-border d-none" role="status" id="loader-icon{{ $list->id }}"
                                        style="width:15px;height:15px">
                                        <span class="visually-hidden">Loading...</span>
                                    </div> 
                                </a>
                                <a href="{{URL::To('billing-page/')}}" class="btn butn text-uppercase add">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-12">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a class="page-link prev" href="javascript:void(0)">Prev</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link active" href="javascript:void(0)">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="javascript:void(0)">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="javascript:void(0)">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link prev" href="javascript:void(0)">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="hidden" name="counterDisplay" id="counterDisplay">1</span>
<form id="frmAddToCart">
    @csrf
    <input type="hidden" name="pqty" id="pqty"/>
    <input type="hidden" name="product_id" id="product_id"/>
</form>


@include('layout/footer')

<script>
    if (screen.width < 992) {
        $('.filters-heading').on('click', function() {
            $(this).siblings('.accordion-display').slideToggle();
        });
    }
</script>
