<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    {{-- <link rel="stylesheet" href="{{asset('css/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script> --}}

    {{-- <style>
        .body-main {
             background: #ffffff;
             border-bottom: 15px solid #1E1F23;
             border-top: 15px solid #1E1F23;
             margin-top: 30px;
             margin-bottom: 30px;
             padding: 40px 30px !important;
             position: relative ;
             box-shadow: 0 1px 21px #808080;
             font-size:10px;
             width: 520px
             
             
             
         }
     
         .main thead {
             background: #1E1F23;
             color: #fff;
             }
         .img{
             height: 100px;}
         h1{
            text-align: center;
         }
     </style> --}}
</head>

<body>
    <div class="container">

        <div class="page-header">
            <h1 style="text-align: center">Invoice Template  </h1>
        </div>
        
        
        <div class="container">
            <div class="row">
                <center>
                <div class="col-md-6 col-md-offset-3" style="font-size: 20px;background: #ffffff;
                border-bottom: 15px solid #1E1F23;
                border-top: 15px solid #1E1F23;
                margin-top: 30px;
                margin-bottom: 30px;
                padding: 40px 30px !important;
                position: relative ;
                box-shadow: 0 1px 21px #808080;
                font-size:10px;
                width: 520px">
                    <div class="col-md-12">
                       <div class="row">
                            <div class="col-md-4">
                                <img class="img" alt="Invoce Template" src="http://pngimg.com/uploads/shopping_cart/shopping_cart_PNG59.png" style="width: 100px;float:left" />
                            </div>
                            <div class="col-md-8" style="float:left">
                                <h4 style="color: #F81D2D;"><strong>Trust Haven Solution</strong></h4>
                                <p>{{ $details['billing_details']['street_address'] }}</p>
                                <p>{{ $details['billing_details']['email'] }}</p>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>INVOICE</h2>
                                <h5>{{ $details['invoice_no'] }}</h5>
                            </div>
                        </div>
                        <br />
                        <div>
                            <table class="table">
                                <thead style="background: #1E1F23;
                                color: #fff;">
                                    <tr>
                                        <th><h5>Product</h5></th>
                                        <th><h5>Qty</h5></th>
                                        <th><h5>Amount</h5></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $total=0;
                                    @endphp
                                    @foreach($details['product'] as $key=>$data)
                                    @php $total=$total+get_product_by_id($data->product_id)->price; @endphp
                                    <tr style="background: rgb(246, 250, 5);margin-bottom:2px">
                                        <td class="col-md-9">{{ get_product_by_id($data->product_id)->name }}</td>
                                        <td class="col-md-9">{{ $data->qty }}</td>
                                        <td class="col-md-3"><i class="fas fa-rupee-sign" area-hidden="true"></i> {{ number_format(get_product_by_id($data->product_id)->price) }} </td>
                                    </tr>
                                    @endforeach

                                    @php
                                    $payble=$details['payble_amount'];
                                    $discount=0;
                                    if(isset($details['code'])){
                                        $discount=$total-$payble;
                                    }
                                    @endphp
                                    <tr>
                                        <td>
                                            <p>
                                                <strong>Total Amount: </strong>
                                            </p>
                                        </td>
                                        <td></td>
                                        <td>
                                            <p>
                                            <strong><i class="fas fa-rupee-sign" area-hidden="true"></i> $ {{ number_format($total) }}</strong>
                                        </p>
                                    </td>
                                    </tr>

                                    <tr>
                                        <td class="text-right">
                                        <p>
                                            <strong>Shipment and Taxes:</strong>
                                        </p>
                                        </td>
                                        <td></td>
                                        <td>
                                        <p>
                                            <strong><i class="fas fa-rupee-sign" area-hidden="true"></i> $  {{ config('app.tax') }} </strong>
                                        </p>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <p>
                                                <strong>Discount: </strong>
                                            </p>
                                        </td>
                                        <td></td>
                                        <td>
                                        <p>
                                            <strong><i class="fas fa-rupee-sign" area-hidden="true"></i> $ {{ number_format($discount) }} </strong>
                                        </p>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>
                                                <strong>Payable Amount:: </strong>
                                            </p>
                                        </td>
                                        <td></td>
                                        <td>
                                        <p>
                                            <strong><i class="fas fa-rupee-sign" area-hidden="true"></i> $ {{ number_format($total) }} </strong>
                                        </p>
                                    </td>
                                    </tr>
                                    <tr style="color: #F81D2D;">
                                        <td class="text-right"><h4><strong>Total:</strong></h4></td>
                                        <td></td>
                                        <td style="width: 52px"><h4><strong><i class="fas fa-rupee-sign" area-hidden="true"></i>$ {{ number_format($payble) }} </strong></h4></td>
                                    </tr>
                                    {{-- @php dd(number_format($payble)); @endphp --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </center>
            </div>
        </div>
    
        </div>
        
</body>
</html>