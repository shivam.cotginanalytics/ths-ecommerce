
@include('layout/head')

@include('layout/header')
<section class="product-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-1 order-lg-0 order-2">
                <div class="change-view">
                    @if($product_images[$product[0]->id][0])
                        @foreach($product_images[$product[0]->id] as $list)
                        <div class="product-img">
                            <img src="{{config('app.trust_haven_app_url').('storage/images/'.$list->images)}}" alt="img" />
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-lg-5 order-lg-0 order-1">
                <div class="product-image-div border position-relative">
                    <div class="like-icon">
                        <button class="share-now">
                            <svg id="share-button-img" width="25" height="22" viewBox="0 0 25 22" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M16.75 5.7619L7.75 9.57143M7.75 12.4286L16.75 16.2381M7.75 11C7.75 12.8409 6.23896 14.3333 4.375 14.3333C2.51104 14.3333 1 12.8409 1 11C1 9.15905 2.51104 7.66667 4.375 7.66667C6.23896 7.66667 7.75 9.15905 7.75 11ZM23.5 4.33333C23.5 6.17428 21.989 7.66667 20.125 7.66667C18.261 7.66667 16.75 6.17428 16.75 4.33333C16.75 2.49238 18.261 1 20.125 1C21.989 1 23.5 2.49238 23.5 4.33333ZM23.5 17.6667C23.5 19.5076 21.989 21 20.125 21C18.261 21 16.75 19.5076 16.75 17.6667C16.75 15.8257 18.261 14.3333 20.125 14.3333C21.989 14.3333 23.5 15.8257 23.5 17.6667Z"
                                    stroke="#B31942" stroke-width="1.5" stroke-linecap="round" />
                            </svg>
                        </button>
                        <div class="share-on position-absolute text-center share-box" id="share-box">
                            <div class="d-flex gap-2 justify-content-center mb-3">
                                {{-- Facebook --}}
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}&display=popup" target="_blank">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="12" fill="#1877F2"/>
                                        <path d="M16.6711 15.4688L17.2031 12H13.875V9.75C13.875 8.80078 14.3391 7.875 15.8297 7.875H17.3438V4.92188C17.3438 4.92188 15.9703 4.6875 14.6578 4.6875C11.9156 4.6875 10.125 6.34922 10.125 9.35625V12H7.07812V15.4688H10.125V23.8547C10.7367 23.9508 11.3625 24 12 24C12.6375 24 13.2633 23.9508 13.875 23.8547V15.4688H16.6711Z" fill="white"/>
                                    </svg>
                                </a>
                                {{-- twitter --}}
                                <a href="https://twitter.com/share?url={{ Request::url() }}&text={{ $product[0]->name }}" target="_blank">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="12" fill="#1D9BF0"/>
                                        <path d="M9.80383 18.3301C15.1258 18.3301 18.0358 13.9201 18.0358 10.0981C18.0358 9.97212 18.0358 9.84612 18.0298 9.72612C18.5938 9.31812 19.0858 8.80812 19.4758 8.22612C18.9598 8.45412 18.4018 8.61012 17.8138 8.68212C18.4138 8.32212 18.8698 7.75812 19.0858 7.08012C18.5278 7.41012 17.9098 7.65012 17.2498 7.78212C16.7218 7.21812 15.9718 6.87012 15.1378 6.87012C13.5418 6.87012 12.2458 8.16612 12.2458 9.76212C12.2458 9.99012 12.2698 10.2121 12.3238 10.4221C9.91784 10.3021 7.78784 9.15012 6.35984 7.39812C6.11384 7.82412 5.96984 8.32212 5.96984 8.85012C5.96984 9.85212 6.47984 10.7401 7.25984 11.2561C6.78584 11.2441 6.34184 11.1121 5.95184 10.8961C5.95184 10.9081 5.95184 10.9201 5.95184 10.9321C5.95184 12.3361 6.94784 13.5001 8.27384 13.7701C8.03384 13.8361 7.77584 13.8721 7.51184 13.8721C7.32584 13.8721 7.14584 13.8541 6.96584 13.8181C7.33184 14.9701 8.39984 15.8041 9.66584 15.8281C8.67584 16.6021 7.42784 17.0641 6.07184 17.0641C5.83784 17.0641 5.60984 17.0521 5.38184 17.0221C6.64784 17.8501 8.17184 18.3301 9.80383 18.3301Z" fill="white"/>
                                    </svg>
                                </a>
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ Request::url() }}" target="_blank">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="12" fill="#0A66C2"/>
                                        <path d="M17.626 5.3999H6.37481C6.11638 5.3999 5.86853 5.50256 5.68579 5.6853C5.50305 5.86804 5.40039 6.11589 5.40039 6.37432V17.6255C5.40039 17.8839 5.50305 18.1318 5.68579 18.3145C5.86853 18.4972 6.11638 18.5999 6.37481 18.5999H17.626C17.8844 18.5999 18.1323 18.4972 18.315 18.3145C18.4977 18.1318 18.6004 17.8839 18.6004 17.6255V6.37432C18.6004 6.11589 18.4977 5.86804 18.315 5.6853C18.1323 5.50256 17.8844 5.3999 17.626 5.3999ZM9.33472 16.6447H7.35014V10.3407H9.33472V16.6447ZM8.34106 9.46715C8.11594 9.46588 7.89624 9.39796 7.70969 9.27195C7.52314 9.14594 7.3781 8.96749 7.29287 8.75913C7.20764 8.55077 7.18605 8.32183 7.23081 8.1012C7.27557 7.88058 7.38468 7.67815 7.54438 7.51948C7.70407 7.36081 7.90719 7.253 8.1281 7.20966C8.34901 7.16631 8.5778 7.18938 8.78561 7.27594C8.99343 7.36251 9.17094 7.50869 9.29575 7.69605C9.42056 7.8834 9.48707 8.10353 9.48689 8.32865C9.48901 8.47937 9.46077 8.62897 9.40385 8.76854C9.34692 8.90812 9.26248 9.0348 9.15555 9.14104C9.04863 9.24728 8.92141 9.33091 8.78147 9.38694C8.64154 9.44296 8.49176 9.47024 8.34106 9.46715ZM16.6497 16.6502H14.6661V13.2062C14.6661 12.1906 14.2343 11.8771 13.677 11.8771C13.0885 11.8771 12.511 12.3207 12.511 13.2319V16.6502H10.5264V10.3453H12.4349V11.2189H12.4606C12.6521 10.8312 13.3231 10.1684 14.3471 10.1684C15.4544 10.1684 16.6506 10.8257 16.6506 12.7507L16.6497 16.6502Z" fill="white"/>
                                    </svg>
                                </a>
                                {{-- Whatsapp --}}
                                <a href="https://api.whatsapp.com/send?url={{ Request::url() }}&text={{ $product[0]->name }}" target="_blank">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="12" fill="url(#paint0_linear_881_790)"/>
                                        <path d="M4.7207 18.9814L5.74964 15.245C5.11351 14.1483 4.7798 12.9063 4.78327 11.6366C4.78327 7.65807 8.03693 4.42334 12.031 4.42334C13.9707 4.42334 15.7922 5.17407 17.1583 6.53716C18.5279 7.90025 19.2822 9.71308 19.2787 11.6401C19.2787 15.6186 16.0251 18.8534 12.0275 18.8534H12.024C10.8109 18.8534 9.61857 18.5489 8.55835 17.9746L4.7207 18.9814ZM8.74258 16.6704L8.96158 16.8018C9.88623 17.3484 10.9464 17.6356 12.0275 17.639H12.031C15.3507 17.639 18.0551 14.9509 18.0551 11.6435C18.0551 10.0417 17.4294 8.53681 16.2927 7.40206C15.156 6.26731 13.6404 5.64458 12.031 5.64458C8.7113 5.64112 6.00687 8.32924 6.00687 11.6366C6.00687 12.7679 6.3232 13.8715 6.92804 14.8264L7.07056 15.0547L6.46224 17.2654L8.74258 16.6704Z" fill="white"/>
                                        <path d="M4.97461 18.7287L5.96878 15.1203C5.35351 14.0652 5.03023 12.8647 5.03023 11.64C5.0337 7.79981 8.17265 4.67578 12.0311 4.67578C13.9048 4.67578 15.6602 5.4023 16.9812 6.71695C18.3021 8.0316 19.0286 9.78216 19.0286 11.6434C19.0286 15.4836 15.8862 18.6076 12.0311 18.6076H12.0277C10.8562 18.6076 9.70562 18.3136 8.68364 17.76L4.97461 18.7287Z" fill="url(#paint1_linear_881_790)"/>
                                        <path d="M4.7207 18.9814L5.74964 15.245C5.11351 14.1483 4.7798 12.9063 4.78327 11.6366C4.78327 7.65807 8.03693 4.42334 12.031 4.42334C13.9707 4.42334 15.7922 5.17407 17.1583 6.53716C18.5279 7.90025 19.2822 9.71308 19.2787 11.6401C19.2787 15.6186 16.0251 18.8534 12.0275 18.8534H12.024C10.8109 18.8534 9.61857 18.5489 8.55835 17.9746L4.7207 18.9814ZM8.74258 16.6704L8.96158 16.8018C9.88623 17.3484 10.9464 17.6356 12.0275 17.639H12.031C15.3507 17.639 18.0551 14.9509 18.0551 11.6435C18.0551 10.0417 17.4294 8.53681 16.2927 7.40206C15.156 6.26731 13.6404 5.64458 12.031 5.64458C8.7113 5.64112 6.00687 8.32924 6.00687 11.6366C6.00687 12.7679 6.3232 13.8715 6.92804 14.8264L7.07056 15.0547L6.46224 17.2654L8.74258 16.6704Z" fill="url(#paint2_linear_881_790)"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M10.2198 8.61976C10.0842 8.31877 9.94168 8.31185 9.81306 8.30839C9.70878 8.30493 9.58711 8.30493 9.46545 8.30493C9.34378 8.30493 9.14912 8.34991 8.98226 8.52981C8.81541 8.70971 8.34961 9.14562 8.34961 10.0347C8.34961 10.9204 8.99965 11.7784 9.09002 11.8995C9.1804 12.0206 10.3449 13.8991 12.1838 14.6222C13.7133 15.2241 14.0261 15.1031 14.3564 15.0719C14.6866 15.0408 15.427 14.636 15.58 14.2139C15.7294 13.7919 15.7294 13.4321 15.6842 13.356C15.639 13.2799 15.5174 13.2349 15.3366 13.1449C15.1559 13.055 14.266 12.6191 14.0991 12.5568C13.9323 12.498 13.8106 12.4668 13.6924 12.6467C13.5708 12.8266 13.2231 13.2314 13.1189 13.3525C13.0146 13.4736 12.9068 13.4874 12.7261 13.3975C12.5453 13.3075 11.9613 13.1172 11.2696 12.5014C10.7308 12.024 10.3658 11.4324 10.2615 11.2525C10.1572 11.0726 10.2511 10.9757 10.3414 10.8858C10.4214 10.8062 10.5222 10.6748 10.6126 10.571C10.7029 10.4672 10.7342 10.3911 10.7933 10.27C10.8524 10.1489 10.8246 10.0451 10.7794 9.95517C10.7342 9.86867 10.3797 8.9761 10.2198 8.61976Z" fill="white"/>
                                        <defs>
                                        <linearGradient id="paint0_linear_881_790" x1="11.9994" y1="23.9988" x2="11.9994" y2="-0.000839698" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#20B038"/>
                                        <stop offset="1" stop-color="#60D66A"/>
                                        </linearGradient>
                                        <linearGradient id="paint1_linear_881_790" x1="12.0012" y1="18.728" x2="12.0012" y2="4.67529" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#20B038"/>
                                        <stop offset="1" stop-color="#60D66A"/>
                                        </linearGradient>
                                        <linearGradient id="paint2_linear_881_790" x1="12.0011" y1="18.98" x2="12.0011" y2="4.42334" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F9F9F9"/>
                                        <stop offset="1" stop-color="white"/>
                                        </linearGradient>
                                        </defs>
                                    </svg>
                                </a>
                                <a href="https://telegram.me/share/url?url={{ Request::url() }}" target="_blank">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="12" fill="url(#paint0_linear_881_791)"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.43202 11.8734C8.93026 10.3493 11.263 9.34452 12.4302 8.85905C15.7627 7.47294 16.4551 7.23216 16.9065 7.22421C17.0058 7.22246 17.2277 7.24706 17.3715 7.36372C17.4929 7.46223 17.5263 7.5953 17.5423 7.6887C17.5583 7.78209 17.5782 7.99485 17.5623 8.1611C17.3818 10.0586 16.6003 14.6633 16.2028 16.7885C16.0346 17.6877 15.7034 17.9892 15.3827 18.0188C14.6859 18.0829 14.1567 17.5582 13.4817 17.1158C12.4256 16.4235 11.8289 15.9925 10.8037 15.3169C9.61896 14.5362 10.387 14.107 11.0622 13.4058C11.2389 13.2222 14.3093 10.4295 14.3687 10.1761C14.3762 10.1444 14.3831 10.0263 14.3129 9.96397C14.2427 9.9016 14.1392 9.92293 14.0644 9.93989C13.9585 9.96393 12.2713 11.0791 9.00276 13.2855C8.52385 13.6143 8.09007 13.7746 7.70141 13.7662C7.27295 13.7569 6.44876 13.5239 5.83606 13.3247C5.08456 13.0804 4.48728 12.9513 4.53929 12.5364C4.56638 12.3203 4.86395 12.0993 5.43202 11.8734Z" fill="white"/>
                                        <defs>
                                        <linearGradient id="paint0_linear_881_791" x1="12" y1="0" x2="12" y2="23.822" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#2AABEE"/>
                                        <stop offset="1" stop-color="#229ED9"/>
                                        </linearGradient>
                                        </defs>
                                    </svg>
                                </a>
                                <a href="https://www.instagram.com/?url={{ Request::url() }}" target="_blank">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="12" fill="#C5338A"/>
                                        <path d="M12.0017 6.09788C13.9256 6.09788 14.1514 6.10645 14.9119 6.14076C15.6151 6.1722 15.9953 6.28941 16.2497 6.38946C16.587 6.52096 16.8272 6.67533 17.0787 6.92689C17.3303 7.17845 17.4875 7.41858 17.6161 7.7559C17.7133 8.01032 17.8334 8.39052 17.8648 9.09375C17.8991 9.85415 17.9077 10.08 17.9077 12.0039C17.9077 13.9277 17.8991 14.1536 17.8648 14.914C17.8334 15.6172 17.7162 15.9974 17.6161 16.2518C17.4846 16.5891 17.3303 16.8293 17.0787 17.0808C16.8272 17.3324 16.587 17.4896 16.2497 17.6183C15.9953 17.7155 15.6151 17.8355 14.9119 17.867C14.1514 17.9013 13.9256 17.9098 12.0017 17.9098C10.0779 17.9098 9.85203 17.9013 9.09163 17.867C8.3884 17.8355 8.00819 17.7183 7.75377 17.6183C7.41645 17.4868 7.17633 17.3324 6.92476 17.0808C6.6732 16.8293 6.51598 16.5891 6.38734 16.2518C6.29014 15.9974 6.17008 15.6172 6.13863 14.914C6.10433 14.1536 6.09575 13.9277 6.09575 12.0039C6.09575 10.08 6.10433 9.85415 6.13863 9.09375C6.17008 8.39052 6.28728 8.01032 6.38734 7.7559C6.51883 7.41858 6.6732 7.17845 6.92476 6.92689C7.17633 6.67533 7.41645 6.5181 7.75377 6.38946C8.00819 6.29227 8.3884 6.1722 9.09163 6.14076C9.85203 6.1036 10.0807 6.09788 12.0017 6.09788ZM12.0017 4.80005C10.0464 4.80005 9.80057 4.80862 9.03159 4.84293C8.26547 4.87723 7.74234 5.00015 7.28495 5.17739C6.81042 5.36035 6.41021 5.60905 6.00999 6.00926C5.60978 6.40947 5.36394 6.81254 5.17812 7.28422C5.00089 7.74161 4.87796 8.26474 4.84366 9.03372C4.80936 9.79984 4.80078 10.0457 4.80078 12.001C4.80078 13.9563 4.80936 14.2022 4.84366 14.9711C4.87796 15.7373 5.00089 16.2604 5.17812 16.7206C5.36108 17.1952 5.60978 17.5954 6.00999 17.9956C6.41021 18.3958 6.81328 18.6417 7.28495 18.8275C7.74234 19.0047 8.26547 19.1276 9.03445 19.1619C9.80343 19.1962 10.0464 19.2048 12.0046 19.2048C13.9628 19.2048 14.2058 19.1962 14.9747 19.1619C15.7409 19.1276 16.264 19.0047 16.7242 18.8275C17.1988 18.6445 17.599 18.3958 17.9992 17.9956C18.3994 17.5954 18.6453 17.1923 18.8311 16.7206C19.0083 16.2633 19.1312 15.7401 19.1655 14.9711C19.1998 14.2022 19.2084 13.9592 19.2084 12.001C19.2084 10.0428 19.1998 9.79984 19.1655 9.03086C19.1312 8.26474 19.0083 7.74161 18.8311 7.28136C18.6481 6.80682 18.3994 6.40661 17.9992 6.0064C17.599 5.60619 17.1959 5.36035 16.7242 5.17453C16.2669 4.9973 15.7437 4.87437 14.9747 4.84007C14.2029 4.80862 13.9571 4.80005 12.0017 4.80005Z" fill="white"/>
                                        <path d="M12.0018 8.30469C9.96075 8.30469 8.30273 9.95985 8.30273 12.0038C8.30273 14.0477 9.9579 15.7029 12.0018 15.7029C14.0458 15.7029 15.7009 14.0477 15.7009 12.0038C15.7009 9.95985 14.0458 8.30469 12.0018 8.30469ZM12.0018 14.4022C10.6754 14.4022 9.60056 13.3273 9.60056 12.0009C9.60056 10.6745 10.6754 9.59966 12.0018 9.59966C13.3283 9.59966 14.4031 10.6745 14.4031 12.0009C14.4031 13.3273 13.3283 14.4022 12.0018 14.4022Z" fill="white"/>
                                        <path d="M15.8467 9.01935C16.3235 9.01935 16.71 8.63283 16.71 8.15604C16.71 7.67924 16.3235 7.29272 15.8467 7.29272C15.3699 7.29272 14.9834 7.67924 14.9834 8.15604C14.9834 8.63283 15.3699 9.01935 15.8467 9.01935Z" fill="white"/>
                                    </svg>
                                </a>
                            </div>
                            <input type="hidden" id="blog_url" value="{{ Request::url() }}">
                            <button type="button" class="copy-link" id="btnCopy">Copy Link</button>
                        </div>
                    </div>
                    <img id="zoom" data-zoom-image="{{config('app.trust_haven_app_url').('storage/images/'.$product[0]->image)}}"
                        src="{{config('app.trust_haven_app_url').('storage/images/'.$product[0]->image)}}" alt="product-image">
                </div>
            </div>
            <div class="col-lg-6 order-lg-0 order-3 product-discription">
                <h2>{{$product[0]->name}}</h2>
                <div>
                    <div class="rate-text d-inline-block me-1">4.5</div>
                    <div class="rating d-inline-block">
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-regular fa-star"></i>
                        <div class="rate-text d-inline-block me-1">(542)</div>
                    </div>
                </div>
                <div class="price-div">
                    <h4 id="display_price">$ {{number_format($product[0]->price,'2','.')
                    }}</h4>
                    <div class="discount-text">
                        <p class="red-color">$ {{number_format($product[0]->mrp,'2','.')}}</p>
                        @php
                        $p_discount=floor(($product[0]->mrp- $product[0]->price)/$product[0]->mrp*100);
                        @endphp
                        <p>Discount {{ $p_discount }}% OFF</p>
                    </div>
                </div>
                <div class="product-description">
                    <ul>
                        <li><strong>Brand:</strong> {{$product[0]->bname}}</li>
                        <li><strong>Model:</strong> {{$product[0]->model}}</li>
                        <li><strong>Ram:</strong> {{$product[0]->size}}</li>
                        <li><strong>Storage:</strong> {{$product[0]->storage}}</li> 
                        <li><strong>Processor:</strong> {{$product[0]->processor}}</li>
                    </ul>
                </div>
                <div>
                    <div class="quantity-counter">
                        <span class="quantity">Quantity :
                            <span class="counter-span">
                                <button id="decrement" class="decrement">-</button>
                                <span class="px-2" name="counterDisplay" id="counterDisplay">1</span>
                                <button id="increment" class="increment">+</button>
                            </span>
                            <input type="hidden" id="product_price" value="{{ $product[0]->price }}">
                            <input type="hidden" id="total" value="{{ $product[0]->price }}">
                        </span>
                    </div>
                </div>
                <div class="pt-3">
                    <a href="javascript:void(0)" onclick="add_to_cart({{$product[0]->id}})" class="btn  butn me-2 text-uppercase add bg-blue contact-button ">Add to cart
                        <div class="spinner-border d-none" role="status" id="loader-icon{{$product[0]->id}}"
                            style="width:15px;height:15px">
                            <span class="visually-hidden">Loading...</span>
                        </div> 
                    </a>
                    <a href="javascript:void(0)" class="btn butn text-uppercase add" onclick="buy_now({{$product[0]->id}})">Buy Now</a>
                    <div id="add_to_cart_msg"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="product-description">
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link nav-btn active" id="home-tab" data-bs-toggle="tab"
                    data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane"
                    aria-selected="true">Product Description</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane"
                    type="button" role="tab" aria-controls="profile-tab-pane"
                    aria-selected="false">Specifications</button>
            </li>
            {{-- <li class="nav-item" role="presentation">
                <button class="nav-link" id="contact-tab" data-bs-toggle="tab"
                    data-bs-target="#contact-tab-pane" type="button" role="tab"
                    aria-controls="contact-tab-pane" aria-selected="false">Reviews</button>
            </li> --}}

        </ul>
        <div class="tab-content tabs-detail-product" id="myTabContent ">
            <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab"
                tabindex="0">
                <div class="row">
                    <div class="col-lg-12">
                        {!!$product[0]->desc!!}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab"
                tabindex="0">
                <div class="row">
                    @foreach($specifications as $key=>$val)
                    <div class="col-lg-6">
                        <h5>{{ get_single_heading($key)->name }}</h5>
                        <ul>
                            @foreach($val as $specification)
                            <li><strong>{{ $specification->title }} : </strong> {{ $specification->content }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab"
                tabindex="0">
                <div class="row">
                    <div class="col-lg-6">
                        <form class="review-form" action="submit">
                            <div class="d-flex mb-3 rating">
                                <i class="fa-solid fa-star me-3 fs-4"></i>
                                <i class="fa-solid fa-star me-3 fs-4"></i>
                                <i class="fa-solid fa-star me-3 fs-4"></i>
                                <i class="fa-solid fa-star me-3 fs-4"></i>
                                <i class="fa-regular fa-star fs-4"></i>
                            </div>
                            <label for="">Your Review</label>
                            <textarea class="review-text form-control" name="" id="" cols="15" rows="10"></textarea>
                            <label class="mt-3">Name</label>
                            <input class="form-control">
                            <label class="mt-3">Your Email</label>
                            <input type="email" class="form-control">

                            <button class="review-submit my-3">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
            {{-- review page --}}

            <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab"
                tabindex="0">

            </div>
            {{-- review page-end --}}
        </div>
    </div>
    </div>
    </div>


</section>



{{-- recomended-sections --}}
<section class="related-products">
    <div class="container">
        <h2 class="head-title"><span class="blue-color">RECOMMEND</span> PRODUCTS</h2>
        <div class="row">
            <div class="col-12">
                <div class="related-product-slider">
                    @if(!$recommend_product->isEmpty())
                    @foreach($recommend_product as $val)
                    <div class="product-list">
                        <figure class="position-relative">
                            <svg class="" width="16" height="22" viewBox="0 0 16 22"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M1 3V20.1384C1 20.5223 1.41474 20.763 1.74807 20.5725L7.00772 17.567C7.62259 17.2157 8.37741 17.2157 8.99228 17.567L14.2519 20.5725C14.5853 20.763 15 20.5223 15 20.1384V3C15 1.89543 14.1046 1 13 1H3C1.89543 1 1 1.89543 1 3Z"
                                    stroke="#B31942" stroke-linecap="round" />
                            </svg>

                        <a href="{{URL::to('product',$val->slug)}}">
                            <img src="{{config('app.trust_haven_app_url').('storage/images/'.$val->image)}}" alt="Product" />
                        </a>
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">{{$val->name}}</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">${{ $val->price }}</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" onclick="add_to_cart({{ $val->id }})" class="btn butn me-2 text-uppercase add bg-blue">Add to cart
                                <div class="spinner-border d-none" role="status" id="loader-icon{{ $val->id }}"
                                    style="width:15px;height:15px">
                                    <span class="visually-hidden">Loading...</span>
                                </div> 
                            </a>
                            <a href="{{URL::to('product',$val->slug)}}" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</section>

<form id="frmAddToCart">
    @csrf
    <input type="hidden" name="pqty" id="pqty"/>
    <input type="hidden" name="product_id" id="product_id"/>
</form>

@include('layout/footer')

<script>
    $('body').click(function(evt){    
    if($(evt.target).is('#share-button-img') || $(evt.target).is('#share-box')) {
        $('.share-box').show();
    }else{
        $('.share-box').hide();
    }
    });


    btnCopy.onclick= ()=> {
        navigator.clipboard.writeText(blog_url.value);
        btnCopy.innerText = "Copied";
    }
</script>

<script>
    $('#zoom').elevateZoom();

    let productImg = document.querySelectorAll('.product-img img');
    productImg.forEach((value)=>{
        value.onclick = ()=> {
            let mainImg = document.getElementById('zoom');
            mainImg.src = value.src;
            $('#zoom').attr('data-zoom-image', value.src);
            $('.zoomWindow').css('background-image', `url(${value.src})`);
        }
    });


</script>
<script>
function buy_now(id){
  var p_price=$('#product_price').val();
  var total_price=$('#total').val();
  var qty=$('#counterDisplay').html();
  var data = new FormData();
  data.append('p_id', id);
  data.append('price', p_price);
  data.append('total_price', total_price);
  data.append('qty', qty);
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
  $.ajax({
    url: "{{ route('billing-page') }}",
    type: "POST",
    data: data,
    contentType: false,
    processData: false,
    success: function(response) {
        location.href="{{ route('billing-page') }}";
    },
    error: function(xhr, textStatus, errorThrown) {
        // Handle error response
    }
  });
}
</script>