@include('layout/head')
<style>
    .accordion-button {
        padding: 0;
    }

    .products-category p {
        color: var(--blue);
        margin-top: 5px;
    }

    .btn-outline-secondary {
        border-left: 0;
        border-right: 1px solid #B31942;
        border-bottom: 1px solid #B31942;
        border-top: 1px solid #B31942;
        border-radius: 4px;

    }

    .search-input {
        border-left: 1px solid #B31942;
        border-top: 1px solid #B31942;
        border-bottom: 1px solid #B31942;
        border-right: 0;
        border-radius: 4px
    }

    .btn-outline-secondary:hover {
        background-color: unset;
        border-color: #B31942
    }

    .btn-outline-secondary:active {
        background: unset
    }

    #button-addon2:active {
        background: unset
    }

    .checked-div{
        background-color: #fffafb;
        border-radius: 0 0 4px 4px;
    }
    .checked-div input[type="radio"] {
        appearance: none;
        width: 15px;
        height: 15px;
        border: 2px solid var(--blue);
        position: relative;
        margin-top: 0.8rem;
        position: relative
    }

    .checked-div input[type="radio"]:checked::before {
        position: absolute;
        content: "";
        background: url(assets/images/check.svg) no-repeat;
        top: 50%;
        width: 90%;
        height: 90%;
        background-position: center;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .products-category .row {
        padding: 1.5rem;
        border-radius: 10px;
    }

    @media(max-width:991px) {
        .accordion-display {
            display: none
        }
    }
    .active_category{
        box-shadow: 0px 2px 7px 0px #dddddd;
    }
    .product-list{
        box-shadow: none !important;
    }
    .product_img{
        height: 200px
    }
    p.product_price_style {
    color: #b31942;
    }
    p.product_stock_style {
    color: #318308;
    }
</style>


@include('layout/header')
{{-- @php dd(config('app.trust_haven_app_url')); @endphp --}}
<section class="product-listing-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-6">
                <h1 class="banner-head">Explore Your Needs</h1>
                <p>Laptops are a quick and easy way to store information from the web. They can be used for gaming,
                    entertainment, travel and business, processing and multitasking, and much more.</p>
            </div>
            <div class="col-lg-4 offset-lg-1 col-md-6">
                <div class="">
                    {{-- @foreach($home_banners[0] as $list)  --}}
                    <figure class="mb-0">
                        <img src="{{config('app.trust_haven_app_url').('storage/images/'.$home_banners[0]->image)}}" alt="{{$home_banners[0]->title}}"/>
                    </figure>
                    {{-- @endforeach --}}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="products-category ">
    <div class="container">
        <div class="row bg-light justify-content-center">
            @foreach(get_catrgory() as $list1)
            <div class="col-lg-3 col-6 text-center mb-lg-0 mb-3">
                <a href="{{ url('/',$list1->category_slug) }}">
                    <button class="bg-transparent border-0">
                        <img src="{{config('app.trust_haven_app_url').('storage/images/'.$list1->category_image)}}" alt="{{$list1->category_name}}" class="@php if($list1->category_slug == collect(request()->segments())->last()) echo 'active_category'; @endphp"/>
                        <p class="mb-0 blue-color">{{$list1->category_name}}</p>
                    </button>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>


<section class="filter-products">
    <div class="container">
        <div class="row">
            {{-- <div class="col-lg-3"> --}}
                {{-- <div class="position-sticky top-20">
                    <div class="input-group mb-3">
                        <input type="text" class="search-input form-control p-2"
                            placeholder="Search your product..." aria-label="Recipient's username"
                            aria-describedby="button-addon2">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i
                                class="fa-solid red-color fa-magnifying-glass"></i></button>
                    </div>
                    <div class="filters-heading text-danger border-bottom border-danger">
                        <h4 class="">Filters <i class="fa-solid fa-chevron-down float-end d-lg-none"></i>
                        </h4>
                    </div>
                    <div class=" accordion accordion-display" id="accordionPanelsStayOpenExample">

                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header  text-danger border-bottom border-danger">
                                <button class="accordion-button accordian-listing-btn" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo">
                                    BRAND
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse show" data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">
                                    <input class="blue-color" type="radio" id="check1" name="check"
                                        value="">
                                    <label class="blue-color" id="check1" for="check1"> Under
                                        <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check2" name="check"
                                        value="">
                                    <label class="blue-color" for="check2">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check3" name="check"
                                        value="">
                                    <label class="blue-color" for="check3">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check4" name="check"
                                        value="">
                                    <label class="blue-color" for="check4">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header  text-danger border-bottom border-danger">
                                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                    PRICE
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">

                                    <input class="blue-color" type="radio" id="check5" name="check"
                                        value="">
                                    <label class="blue-color" for="check5"> Under <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check6" name="check"
                                        value="">
                                    <label class="blue-color" for="check6">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check7" name="check"
                                        value="">
                                    <label class="blue-color" for="check7">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check8" name="check"
                                        value="">
                                    <label class="blue-color" for="check8">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>


                                </div>
                            </div>
                        </div>
                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header  text-danger border-bottom border-danger">
                                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true"
                                    aria-controls="collapseFour">
                                    DISPLAY SIZE
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse "
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">
                                    <input class="blue-color" type="radio" id="check9" name="check"
                                        value="">
                                    <label class="blue-color" for="check9"> Under <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check10" name="check"
                                        value="">
                                    <label class="blue-color" for="check10">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check11" name="check"
                                        value="">
                                    <label class="blue-color" for="check11">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check12" name="check"
                                        value="">
                                    <label class="blue-color" for="check12">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header  text-danger border-bottom border-danger">
                                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="true"
                                    aria-controls="collapseFive">
                                    STORAGE
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse "
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">
                                    <input class="blue-color" type="radio" id="check13" name="check"
                                        value="">
                                    <label class="blue-color" for="check13"> Under <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check14" name="check"
                                        value="">
                                    <label class="blue-color" for="check14">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check15" name="check"
                                        value="">
                                    <label class="blue-color" for="check15">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check16" name="check"
                                        value="">
                                    <label class="blue-color" for="check16">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header  text-danger border-bottom border-danger">
                                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="true"
                                    aria-controls="collapseSix">
                                    PROCESSOR
                                </button>
                            </h2>
                            <div id="collapseSix" class="accordion-collapse collapse "
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">
                                    <input class="blue-color" type="radio" id="check17" name="check"
                                        value="">
                                    <label class="blue-color" for="check17"> Under <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check18" name="check"
                                        value="">
                                    <label class="blue-color" for="check18">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check19" name="check"
                                        value="">
                                    <label class="blue-color" for="check19">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check20" name="check"
                                        value="">
                                    <label class="blue-color" for="check20">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header  text-danger border-bottom border-danger">
                                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseSeven"
                                    aria-expanded="true" aria-controls="collapseSeven">
                                    RAM SIZE
                                </button>
                            </h2>
                            <div id="collapseSeven" class="accordion-collapse collapse "
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">
                                    <input class="blue-color" type="radio" id="check21" name="check"
                                        value="">
                                    <label class="blue-color" for="check21"> Under <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check23" name="check"
                                        value="">
                                    <label class="blue-color" for="check23">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check24" name="check"
                                        value="">
                                    <label class="blue-color" for="check24">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check25" name="check"
                                        value="">
                                    <label class="blue-color" for="check25">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item accordion-listing-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button accordian-listing-btn collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#collapseEight"
                                    aria-expanded="true" aria-controls="collapseEight">
                                    DISCOUNT
                                </button>
                            </h2>
                            <div id="collapseEight" class="accordion-collapse collapse "
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body checked-div">
                                    <input class="blue-color" type="radio" id="check26" name="check"
                                        value="">
                                    <label class="blue-color" for="check26"> Under <span>&nbsp</span>
                                        <span>&nbsp</span>-<span>&nbsp</span> <span>&nbsp</span> $500</label><br>
                                    <input class="blue-color" type="radio" id="check27" name="check"
                                        value="">
                                    <label class="blue-color" for="check27">$500 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span> $600</label><br>
                                    <input class="blue-color" type="radio" id="check28" name="check"
                                        value="">
                                    <label class="blue-color" for="check28">$600 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>$750</label><br>
                                    <input class="blue-color" type="radio" id="check29" name="check"
                                        value="">
                                    <label class="blue-color" for="check29">$750 <span>&nbsp</span>
                                        <span>&nbsp</span> - <span>&nbsp</span> <span>&nbsp</span>
                                        $900</label><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- @include('layout.filter') --}}
            {{-- </div> --}}
            <div class="col-lg-12">
                <div class="row row-gap-4 mb-4 justify-content-center">
                    {{-- @php dd($home_products); @endphp --}}
                    @foreach($home_products as $list)
                    <div class="col-lg-3 col-md-6">
                        <div class="product-list text-center">
                            <figure class="position-relative product_img">
                                <a href="{{URL::to('product',$list->slug)}}">
                                    <img src="{{config('app.trust_haven_app_url').('storage/images/'.$list->image)}}" alt="{{$list->name}}" />
                                </a>
                            </figure>
                            <div class="prod-name mb-2" title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                                <a href="{{URL::to('product',$list->slug)}}" class="blue-color">{{$list->name}}</a>
                                
                            </div>
                           <p class="product_price_style">$ {{ number_format($list->price,'2','.')}}</p>
                               <p class="product_stock_style"> In stock</p>
                            <div>
                                <a href="javascript:void(0)" onclick="add_to_cart({{ $list->id }})" class="btn butn me-2 text-uppercase add bg-blue w-100">Add to cart
                                    <div class="spinner-border d-none" role="status" id="loader-icon{{ $list->id }}"
                                        style="width:15px;height:15px">
                                        <span class="visually-hidden">Loading...</span>
                                    </div> 
                                </a>
                                {{-- <a href="{{URL::To('billing-page/')}}" class="btn butn text-uppercase add">Buy Now</a> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{-- <div class="row">
                    <div class="col-12">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item">
                                    <a class="page-link prev" href="javascript:void(0)">Prev</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link active" href="javascript:void(0)">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="javascript:void(0)">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="javascript:void(0)">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link prev" href="javascript:void(0)">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</section>

{{-- <section class="related-products">
    <div class="container">
        <h2 class="head-title"><span class="blue-color">RECOMMEND</span> PRODUCTS</h2>
        <div class="row">
            <div class="col-12">
                <div class="related-product-slider">
                    <div class="product-list">
                        <figure class="position-relative">


                            <a><img src="./assets/images/product-listing/product-1.jpg" alt="Product" /></a>
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">HP 15s-dr3506TU Intel Core i3 11th Gen
                                (15.6 inch,8GB, 1TB and 256GB)</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">$900</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="btn butn me-2 text-uppercase add bg-blue">Add to
                                cart</a>
                            <a href="javascript:void(0)" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                    <div class="product-list">
                        <figure class="position-relative">


                            <img src="./assets/images/product-listing/product-1.jpg" alt="Product" />
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">HP 15s-dr3506TU Intel Core i3 11th Gen
                                (15.6 inch,8GB, 1TB and 256GB)</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">$900</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="btn butn me-2 text-uppercase add bg-blue">Add to
                                cart</a>
                            <a href="javascript:void(0)" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                    <div class="product-list">
                        <figure class="position-relative">


                            <img src="./assets/images/product-listing/product-1.jpg" alt="Product" />
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">HP 15s-dr3506TU Intel Core i3 11th Gen
                                (15.6 inch,8GB, 1TB and 256GB)</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">$900</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="btn butn me-2 text-uppercase add bg-blue">Add to
                                cart</a>
                            <a href="javascript:void(0)" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                    <div class="product-list">
                        <figure class="position-relative">


                            <img src="./assets/images/product-listing/product-1.jpg" alt="Product" />
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">HP 15s-dr3506TU Intel Core i3 11th Gen
                                (15.6 inch,8GB, 1TB and 256GB)</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">$900</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="btn butn me-2 text-uppercase add bg-blue">Add to
                                cart</a>
                            <a href="javascript:void(0)" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                    <div class="product-list">
                        <figure class="position-relative">

                            <img src="./assets/images/product-listing/product-1.jpg" alt="Product" />
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">HP 15s-dr3506TU Intel Core i3 11th Gen
                                (15.6 inch,8GB, 1TB and 256GB)</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">$900</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="btn butn me-2 text-uppercase add bg-blue">Add to
                                cart</a>
                            <a href="javascript:void(0)" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                    <div class="product-list">
                        <figure class="position-relative">


                            <img src="./assets/images/product-listing/product-1.jpg" alt="Product" />
                        </figure>
                        <div class="prod-name mb-2"
                            title="HP 15s-dr3506TU Intel Core i3 11th Gen (15.6 inch,8GB, 1TB and 256GB)">
                            <a href="javascript:void(0)" class="blue-color">HP 15s-dr3506TU Intel Core i3 11th Gen
                                (15.6 inch,8GB, 1TB and 256GB)</a>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div>
                                <div class="price d-inline-block me-1">$900</div>
                                <div class="stock red-color d-inline-block">In stock</div>
                            </div>
                            <div>
                                <div class="rate-text d-inline-block me-1">4.5</div>
                                <div class="rating d-inline-block">
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-solid fa-star"></i>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <i class="fa-regular fa-star"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="btn butn me-2 text-uppercase add bg-blue">Add to
                                cart</a>
                            <a href="javascript:void(0)" class="btn butn text-uppercase add">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
{{-- <input class="hidden" id="counterDisplay" value="1" /> --}}
<form id="frmAddToCart">
    @csrf
    <input type="hidden" name="pqty" id="pqty"/>
    <input type="hidden" name="product_id" id="product_id"/>
</form>


@include('layout/footer')

<script>
    if (screen.width < 992) {
        $('.filters-heading').on('click', function() {
            $(this).siblings('.accordion-display').slideToggle();
        });
    }
</script>
