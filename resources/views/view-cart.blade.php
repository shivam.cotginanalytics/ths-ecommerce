@include('layout/head')
<style>
    .empty-cart {
        width: 100%;
        height: 100vh;
        display: flex;
        left: 50%;
        top: 30%;
        position: relative;
        display: none;
    }
    .view-cart{
        padding: 60px 0;
    }
    .view-cart-box {
        background: #F8F8F8;
        border-radius: 2px;
        padding: 30px;
    }
    .remove-cart-btn {
        position: relative;
        float: right;
        width: 20px;
        top: 0;
        right: 0;
    }
    .view-cart-box .stock  {
        font-weight: 500;
        line-height: 21px;
        margin-right: 35px;
        margin-bottom: 0;
    }
    .view-cart-box .product-brands li strong {
        font-weight: 500;
    }
    .view-cart-box .product-brands li {
        font-size: 14px;
    }
    .quantity-counter span.quantity {
        font-weight: 500;
        font-size: 14px;
        line-height: 16px;
    }
    
    .view-cart-box .date-delivery span{
        font-weight: 500;
        line-height: 16px;
        color: #333333;
    }
    .view-cart-box p.date-delivery{
        font-size: 14px !important;
    }
    .view-cart-box .price-product h4,
    .view-cart-box .price-product h4 span {
        font-weight: 600;
        line-height: 10px;  
        font-size: 30px !important;    
    }
    .view-cart-box .price-product span {
        color: #B31942;
        font-size: 18px;
        font-weight: 500;
    }
    .view-cart-box .review-star {
        margin-left: 30px;
    }
    /* .view-cart-box .product-images img {
        width: 160px;
    } */
    .order-summary-header h4 {
        font-weight: 500;
        font-size: 20px;
        line-height: 23px;
        color: #FFFFFF;
        text-align: center;
        margin: 0;
    }   
    .order-summary-box {
        border-radius: 4px;
        margin-top: 48px;
        position: sticky;
        top: 20%;
    }
    .order-summary {
        background: #F5F5F5;
    }
    .order-summary-header {
        background: #B31942;
        border-radius: 2px;
        padding: 17px 0;
    }
    .order-summary-detail li strong {
        font-weight: 500;
    }
    .order-summary-detail ul li {
        display: flex;
        justify-content: space-between;
    }
    .order-summary-detail {
        padding: 30px;
    }
    .order-summary-detail li {
        margin-bottom: 12px;
    }
    .order-summary-detail li.total-amount {
        border-top: 1px solid #333333;
        border-bottom: 1px solid #333333;
        padding: 12px 0;
    }
    .order-summary-box a.order-button {
        background: #B31942;
        border-radius: 4px;
        font-weight: 500;
        font-size: 20px;
        line-height: 2.8;
        text-align: center;
        color: #FFFFFF;
        width: 100%;
        display: block;
        height: 60px;
        border: 1px solid var(--red);
    }
    .order-summary-box .total-amount span {
        font-weight: 500;
        font-size: 16px;
        color: #333333;
    }
    .order-summary-box .total-amount strong {
        font-weight: 500;
        font-size: 16px;
        line-height: 19px;
        color: #B31942;
    }
    .order-summary-box a.order-button:hover{
        color: var(--blue);
        background-color: var(--white);
    } 
    .empty_product{
        background: #b31942;
        color: white;
    }
    @media (max-width:991px){
        .view-cart-box .product-images img {
            width: 60%;
            margin: auto;
            display: flex;
        }
        .order-summary-box {
            margin-top: 0;
        }
    }
    @media (max-width:767px){
        .view-cart-box {
            padding: 20px 10px;
        }
        .view-cart-box .review-star {
            margin-left: 0px;
        }
        .view-cart-box .stock {
            margin-right: 15px;

        }
        .view-cart-box .row div {
            margin: 0;
            padding: 5px;
        }
        .view-cart-box .row div {
            flex-wrap: wrap;
        }
        .order-summary-detail {
            padding: 1rem;
        }
    }
</style>
@include('layout/header')
<div class="view-cart">
    <div class="container">
        @if(count(get_user_card())>0)
        <div class="row" id="display_priduct">
            <div class="col-lg-8">
                <h2><span class="blue-color">View </span><span class="red-color">Cart</span></h2>
                @php $total_price=0; @endphp
                @foreach(get_user_card() as $data)
                @php
                // dd(get_product_by_id($data->product_id));
                $cart_data=get_cart($data->product_id);
                if($cart_data->qty_price){
                    $total_price=$total_price+$cart_data->qty_price;
                }else{
                 $total_price=$total_price+$data->product_price;
                }
                @endphp
                <div class="view-cart-box mb-4">
                    <a href="javascript:void(0)" class="cart-remove-button" onclick="DeleteCartProduct({{$data->id}})">
                        <img class="remove-cart-btn" src="{{asset('assets/images/remove-1.svg')}}" alt="img">
                    </a>
                    <div class="row row-gap-4">
                        <div class="col-lg-3">  
                            <a href="#" class="product-images">
                                <img src="{{config('app.trust_haven_app_url').('storage/images/'.get_product_by_id($data->product_id)->image)}}" alt="">
                            </a>
                        </div>
                        <div class="col-lg-9">
                            <a href="#">
                                <h4>{{get_product_by_id($data->product_id)->name}}</h4>
                            </a>
                            <div class="d-flex align-items-center justify-content-between">
                                <div>
                                    <div class="quantity-counter mb-4">
                                        <span class="quantity">Quantity :
                                            {{-- <span onchange="UpdateQty('{{get_product_by_id($data->product_id)->id}}','{{get_product_by_id($data->product_id)->price}}')" class="counter-span"> --}}
                                                <span class="counter-span">
                                                <button class="decrement" onclick="decrement_product({{ get_product_by_id($data->product_id)->id }})">-</button>
                                                <span class="px-2" id="counterDisplay{{ get_product_by_id($data->product_id)->id }}">{{ get_cart(get_product_by_id($data->product_id)->id)->qty }}</span>
                                                <button class="increment" onclick="increment_product({{ get_product_by_id($data->product_id)->id }})">+</button>
                                                <input type="hidden" id="product_price{{ get_product_by_id($data->product_id)->id }}" value="{{ get_product_by_id($data->product_id)->price }}">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="price-product pt-3">
                                    <h4 id="display_price{{ get_product_by_id($data->product_id)->id }}">$ <span>{{ get_cart(get_product_by_id($data->product_id)->id)->qty_price != null? number_format(get_cart(get_product_by_id($data->product_id)->id)->qty_price):number_format(get_cart(get_product_by_id($data->product_id)->id)->product_price) }} </span></h4>
                                    <p>$<del> {{get_product_by_id($data->product_id)->mrp}} </del><span>23% off</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-lg-4">
                <div class="order-summary-box">
                    <div class="order-summary">
                        <div class="order-summary-header">
                            <h4>Order Summary</h4>
                        </div>
                        <div class="order-summary-detail">
                            <ul>
                                {{-- <li><strong>Brand : </strong> <span>HP</span></li>
                                <li><strong>Color : </strong> <span>Mat Black</span></li>
                                <li><strong>Category : </strong> <span>Notebook</span></li>
                                <li><strong>Shipping : </strong> <span>Standard</span></li> --}}
                                {{-- <li><strong>Warranty : </strong> <span>1 Year Warranty</span></li> --}}
                                <li class="total-amount"><strong>SubTotal Amount : </strong>  <span id="total_amount">$ {{ number_format($total_price) }}</span></li>
                            </ul>
                            <a href="{{ route('billing-page') }}" class="order-button">Order Proceed</a>
                            <a href="{{ route('billing-page') }}?type=1" class="order-button d-none inquiry-button">Enquire Now</a>
                        </div>
                    </div>
                    <div class="d-flex  gap-3 py-3">
                        <span>
                            <svg width="21" height="25" viewBox="0 0 21 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.025 0L0 4.54514V11.3646C0 17.6694 4.27778 23.5674 10.025 25C15.7729 23.5681 20.05 17.6701 20.05 11.3639V4.54444L10.025 0Z" fill="#B31942"/>
                                <path d="M14.0688 10L8.23478 15.834C8.01343 16.0553 7.65455 16.0553 7.4332 15.834L5 13.4008" stroke="white" stroke-linecap="round"/>
                            </svg>
                        </span>                                 
                        <p class="mb-0">Safe and Secure Payments Easy returns 100% Authentic products.</p>    
                    </div>
                </div>
                
            </div>
        </div>
        @else 
        <div class="card text-center">
            <div class="card-header empty_product">
              Empty
            </div>
            <div class="card-body">
              <h5 class="card-title">Your cart is Empty!</h5>
              <p class="card-text">Please go to the home page or product page!</p>
              <a href="{{ URL::to('/') }}" class="">Go Back</a>
            </div>
            <div class="card-footer text-muted">
              {{-- 2 days ago --}}
            </div>
          </div>
        @endif
    </div>
</div>
<form id="frmAddToCart">
    @csrf
    <input type="hidden" name="pqty" id="pqty"/>
    <input type="hidden" name="product_id" id="product_id"/>
</form>

@include('layout/footer')
<script>
    $('.cart-remove-button').click(function(){
        $(this).parent('.view-cart-box').remove();
    });
</script>