@include('layout/head')
<style>
    .billing .form-control:focus {
        border: 0;
        border-bottom: 1px solid var(--red);
    }
    .aboutUs {
        margin: 10px;
        background-color: var(--red);
    }
    
    .product-bill-details {
        padding: 2.5rem;
        
        
        border-radius: 2px;
        background-color: white;
    }
    
    .btns-billing {
        text-align: center;
        
    }
    
    .about-heading {
        color: var(--white);
        font-weight: 700;
        font-size: 40px;
        line-height: 47px;
        display: flex;
        justify-content: center;
        position: relative;
        text-align: center;
    }
    
    .bill-input {
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
        border-radius: 0px;
        background-color: #FAFAFA;
    }
    
    .prod-img {
        width: 30%;
        height:
        19%
    }
    .bill-select{
        border-top: 0px;
        border-right: 0px;
        border-left: 0px;
        border-radius: 0px;
        background-color:#FAFAFA;
    }
    
    .text-zone ::placeholder{
        
    }
    
    .coupan-code {
        border: 1px solid #DEDEDE;
        border-radius: 2px;
        width: 50%
    }
    
    .apply-btn {
        width: 30%;
        border: none;
        background-color: var(--red);
        color: white;
        border-radius: 2px
    }
    
    .make-my-payment {
        background: var(--red);
        color: white;
        border: none;
        border-radius: 2px
    }
    
    .payment-btn {
        text-align: center;
        
    }
    
    .text-zone {
        border: 0.5px solid #D9D9D9;
        border-radius: 4px;
        width: 100%;
        color: #1e1e1e;
        padding: 15px
    }
    .billing-container{
        background: #FAFAFA;
        padding: 2.5rem 1.5rem 2.5rem 1.5rem
    }
    .billing-container .form-select:focus {
        border-color:var(--red);
        outline: 0;
        box-shadow: none;
    }
    .billing-container textarea:focus-visible{
        border-color: var(--red);
        outline: 0;
    }
    .product-bill-details td a {
        font-weight: 500;
        font-size: 16px;
        line-height: 19px;
        color: #333333;
    }
    .product-bill-details .table th {
        font-weight: 600;
    }
    .billing input[type="checkbox"] {
        appearance: none;
        width: 15px;
        height: 15px;
        border: 2px solid var(--blue);
        position: relative;
    }
    .billing input[type="checkbox"]:checked::before {
        position: absolute;
        content: "";
        background: url(../assets/images/check.svg) no-repeat;
        top: 50%;
        width: 90%;
        height: 90%;
        background-position: center;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .form-check {
        align-items: center;
        column-gap: 10px;
        padding-left: 4px;
    }
    .alert.alert-success.mx-auto.py-0 {
        text-align: start;
        width: 40%;
        padding-inline: 0.5rem;
        border: 0;
    }
    .btn-cls {
        margin-left: 0.5rem;
    }
    #billing_address .nav-pills .nav-link {
        color: var(--bs-black);
    }
    #billing_address .nav-pills .nav-link.active {
        color: var(--bs-white);
        background: var(--red);
    }
    #accountType {
        border-radius: 0;
        background: transparent;
        border-inline: 0;
        border-top: 0;
        background: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='m2 5 6 6 6-6'/%3e%3c/svg%3e) no-repeat;
        background-size: 4%;
        background-position: 97% center;
    }
    @media(max-width:767px) {
        .coupan-code {
            font-size: 10px
        }

        .butn{
            padding: 5px 18px;
        }
        
        .apply-btn {
            font-size: 10px;
            margin: auto;
            
        }
        .call-button{
            margin: auto
        }
        
        .payment-btn {
            font-size: 10px;
           margin-bottom: 20px;
        }
        .btns-billing{
            display: flex;
            flex-direction: column;
            gap: 15px
        }

        .btns-billing input{
            margin: auto;
            width: 50%;
        }

        .table tr{
            display: flex;
            flex-direction: column;
        position: relative;
        }

        .table tr td svg{
            position: absolute;
                top: -25px;
            right: 0px;
        }

        .table tr td{
            border: none
        }

        .product-bill-details {
            padding: 0.5rem;
            border: 1px solid #DEDEDE;
            border-radius: 2px;
        }

    }
</style>

@include('layout/header')
<section class="aboutUs m-0 p-10">
    <h1 class="about-heading m-0 p-0">BILLING DETAILS</h1>
</section>

<div class="container py-3">
    <nav style="--bs-breadcrumb-divider: '>>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ URL::To('/') }}">
                    <svg width="23" height="21" viewBox="0 0 23 21" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                    d="M1 8.94369L10.0675 1.51755C10.9101 0.827482 12.0899 0.827482 12.9325 1.51755L22 8.94369M4.5 6.07724V17.543C4.5 18.9 5.54467 20 6.83333 20H8H15H16.1667C17.4553 20 18.5 18.9 18.5 17.543V1.57281M9.16667 20V12.6291C9.16667 11.9507 9.689 11.4006 10.3333 11.4006H12.6667C13.311 11.4006 13.8333 11.9507 13.8333 12.6291V20"
                    stroke="#B31942" stroke-width="1.5" stroke-linecap="round" />
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item active" aria-current="page"><span class="red-color">Solutions</span> </li>
        <li class="breadcrumb-item active" aria-current="page"><span class="blue-color">Billing Details</span></li>
    </ol>
</nav>
</div>

@php
    $product_arr=[];
    $product_details_arr=[];
    $total=config('app.tax');
    $sub_total=0;
    $payble_amount='';
    $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
@endphp
@if(count(get_user_card()) > 0)
@foreach(get_user_card() as $key=>$val)
@php 
if(get_cart($val->product_id)->qty_price){
$total=$total+get_cart($val->product_id)->qty_price;
$sub_total=$sub_total+get_cart($val->product_id)->qty_price;
$product_details_arr['qty']=get_cart($val->product_id)->qty;
$product_details_arr['product_id']=$val->product_id;
}else{
    $total=$total+get_cart($val->product_id)->product_price;
    $sub_total=$sub_total+get_cart($val->product_id)->product_price;
    $product_details_arr['qty']=1;
    $product_details_arr['product_id']=$val->product_id;
}
$payble_amount=$total;
$product_arr[$key]=$product_details_arr;
@endphp
@endforeach
@endif
{{-- @php dd($product_arr); @endphp --}}
<section class="billing">
    <div class="container billing-container ">
        <div class="row">
            <div class="col-lg-7">
                @if(session('success_msg'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success_msg') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                      </div>
                    @endif
                    @if(session('error_msg'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error_msg') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                      </div>
                    @endif
                @if($type == 0 && $total < 3000)
                <h2 class="mb-4"><span class="blue-color">Payment Information</span></h2>
                @endif
                <form method="post" class="row" action="@if($type == 0 && $total < 3000){{ route('make-payment') }} @else {{ route('store-enquire') }} @endif" id="billing_address">
                    <input type="hidden" id="payment_type" name="payment_type" value="1">
                    <input type="hidden" name="payble_amount" id="payble_amount" value="{{ $payble_amount }}">
                    <input type="hidden" name="product" value="{{ json_encode($product_arr) }}">
                    <input type="hidden" name="code" value="" id="code">
                    {{-- choose payment method --}}
                    <div class="col-12 mb-5">
                        {{-- <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                              <button class="nav-link active" id="card-detail-tab" data-bs-toggle="pill" data-bs-target="#card-detail" type="button" role="tab" aria-controls="card-detail" aria-selected="true">Credit / Debit Card</button>
                            </li>
                            <li class="nav-item" role="presentation">
                              <button class="nav-link" id="account-detail-tab" data-bs-toggle="pill" data-bs-target="#account-detail" type="button" role="tab" aria-controls="account-detail" aria-selected="false">Bank Account</button>
                            </li>
                          </ul> --}}
                          @if($type == 0 && $total < 3000)
                          <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="card-detail" role="tabpanel" aria-labelledby="card-detail-tab" tabindex="0">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="cardNumber">Card Number</label>
                                        <input type="text" name="card_number" value="{{ old('card_number') }}" id="cr_no" class="bill-input form-control" placeholder="xxxx-xxxx-xxxx-xxxx" minlength="19" maxlength="19" onkeyup="card_number(this.value)"/>
                                        @error('card_number')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="cardNumber">Card Holder Name</label>
                                        <input type="text" name="card_holder_name" id="card_holder_name" class="bill-input form-control" placeholder="Name" />
                                        @error('card_holder_name')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="expDate">Expiry Date</label>
                                        {{-- <input type="text" name="exp_date" id="expDate" class="bill-input form-control" placeholder="MM/YY" /> --}}
                                        <div class="row">
                                        <div class="col-6">
                                        <select name="month" id="" class="bill-input form-control">
                                            @foreach($months as $k=>$val)
                                            <option value="{{ $k }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                        <div class="col-6">
                                        <select name="year" id="" class="bill-input form-control">
                                            @for($i = date('Y'); $i <= (date('Y') + 15); $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>            
                                            @endfor
                                        </select>
                                        </div>
                                        @error('month')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                        @error('year')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cvvNumber">CVV</label>
                                        <input type="password" name="cvv" id="cvvNumber" class="bill-input form-control" maxlength="3" placeholder="" />
                                        @error('cvv')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="account-detail" role="tabpanel" aria-labelledby="account-detail-tab" tabindex="0">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="bankName">Bank Name</label>
                                        <input type="text" name="bank_name" id="bankName" class="bill-input form-control" />
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="bankAcc">Bank Account Number</label>
                                        <input type="text" name="account_number" id="bankAcc" class="bill-input form-control" />
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="accountType" class="mt-4 mb-1">Bank Account Type</label>
                                        <select name="account_type" id="accountType" class="form-select">
                                            <option value="Personal Checking">Personal Checking</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                          </div>
                          @endif
                    </div>

                    <h2><span class="blue-color">@if($type == 0 && $total < 3000)Billing Details @else Send Query @endif</span></h2>
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">First Name<span class="red-color"> *</span></label>
                        <input value="{{ old('fname') }}" class="bill-input form-control" type="text" name="fname" />
                        @error('fname')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">Last Name</label>
                        <input value="{{ old('lname') }}" type="text" class="bill-input form-control" name="lname" />
                        @error('lname')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">Company / Region<span class="red-color"> *</span></label>
                        <input value="{{ old('region') }}" type="text" class=" bill-input form-control" name="region" placeholder="India" />
                        @error('region')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">Street address<span class="red-color"> *</span></label>
                        <input value="{{ old('street_address') }}" type="text" class=" bill-input form-control" name="street_address" placeholder="House number and street name" />
                        @error('street_address')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">Town/City<span class="red-color"> *</span></label>
                        <input value="{{ old('city') }}" type="text" class=" bill-input form-control" name="city" />
                        @error('city')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">State<span class="red-color"> *</span></label>
                        <input value="{{ old('state') }}" type="text" class=" bill-input form-control" name="state" />
                        @error('state')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">Pin Code<span class="red-color"> *</span></label>
                        <input value="{{ old('pin_code') }}" type="text" class="bill-input form-control" name="pin_code" placeholder=" " />
                        @error('pin_code')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="mt-4 mb-1">Email address<span class="red-color"> *</span></label>
                        <input value="{{ old('email') }}" type="text" class="bill-input form-control" name="email" placeholder=" " />
                        @error('email')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-12">
                        <label class="mt-4 mb-1">Company name: (Optional)</label>
                        <input value="{{ old('company') }}" type="text" class="bill-input form-control" name="company" placeholder=" " />
                        @error('company')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-12">
                        {{-- <label class="mt-4 mb-3">Order note: (Optional)</label>
                        <textarea placeholder="Notes about your order, e.g. special notes for delivery" class="text-zone" name="order_note" id="" cols="" rows="7"></textarea> --}}
                        <div class="form-check mt-4 mb-1 d-flex">
                            <span><input type="checkbox" id="flexCheckDefault" name="agree"></span>
                            <label class="form-check-label" for="flexCheckDefault">
                                I have read and agree to Trust haven solution 
                                <a href="#" class="red-color">Terms of service</a>
                                and <a href="#" class="red-color">Privacy Policy</a>.
                            </label>
                        </div>
                        @error('agree')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="col-lg-5">
                <div class="product-bill-details">
                    <h3><span class="red-color">Products</span></h3>
                    <table class="table">
                        <tbody>
                            @if(count(get_user_card()) > 0)
                            @foreach(get_user_card() as $val)
                            <tr class="product">
                                <th scope="row"><a href="#"><img src="{{ config('app.trust_haven_app_url').('storage/images/'.get_product_by_id($val->product_id)->image) }}" alt=""></a></th>
                                <td><a href="#">{{get_product_by_id($val->product_id)->name}}</a>
                                    <p><span>QTY     </span>: {{ get_cart($val->product_id)->qty }}</p>
                                </td>
                                <td>${{ get_cart($val->product_id)->qty_price != null?number_format(get_cart($val->product_id)->qty_price):number_format(get_cart($val->product_id)->product_price) }}</td>
                                <td class="close"><span class="red-color " onclick="DeleteCartProduct({{$val->id}})"><i class="fa-solid fa-circle-xmark"></span></i></td>
                            </tr>
                            @endforeach
                            @else
                            <script>window.location = "{{ url('/') }}";</script>
                            @endif
                        </tbody>
                    </table>
                    <table class="table">
                        
                        <tbody>
                            <tr>
                                <th scope="row">Sub Total</th>
                                
                                <td class="text-end" colspan="3">${{ number_format($sub_total) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Tax</th>
                                
                                <td class="text-end" colspan="3">$ {{ config('app.tax') }}.00</td>
                            </tr>
                            <tr>
                                <th scope="row">Discount Coupon</th>
                                
                                <td class="text-end" colspan="3" id="discount">$ 0.00</td>
                            </tr>
                            <tr>
                                <th scope="row">Total</th>
                                
                                <td class="text-end" colspan="3" id="total_price">${{number_format($total) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @if($type == 0 && $total < 3000)
                    <div class="btns-billing pt-3">
                        <input value="" type="text" class="coupan-code p-2" placeholder="Coupon Code" id="coupon_code">
                        <a class="call-button butn" href="javascript::void()" onclick="apply_coupon()">Apply</a>
                        <p class="" id="coupon_message"></p>
                        <div class="alert alert-success py-0 mx-auto d-none" role="alert" id="applied_message"><span class="message">Coupon Applied</span>
                            <span role="button" class="btn-cls" onclick="coupon_close()">X</span>
                        </div>
                    </div>
                    @endif
                    <div class="payment-btn pt-4 mt-4">
                        @if($type == 0 && $total < 3000)
                        <a class="call-button butn " href="javascript::void()" onclick="make_payment()">Make Payment</a>
                        @else
                        <a class="call-button butn " href="javascript::void()" onclick="make_payment()">Send query</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" id="apply_coupon_url" value="{{ route('apply-coupon') }}">

@include('layout/footer');

<script>
    // Disable right-click
document.addEventListener('contextmenu', (e) => e.preventDefault());

function ctrlShiftKey(e, keyCode) {
  return e.ctrlKey && e.shiftKey && e.keyCode === keyCode.charCodeAt(0);
}

document.onkeydown = (e) => {
  // Disable F12, Ctrl + Shift + I, Ctrl + Shift + J, Ctrl + U
  if (
    event.keyCode === 123 ||
    ctrlShiftKey(e, 'I') ||
    ctrlShiftKey(e, 'J') ||
    ctrlShiftKey(e, 'C') ||
    (e.ctrlKey && e.keyCode === 'U'.charCodeAt(0))
  )
    return false;
};
</script>


<script>
    $('.close').click(function(){
        $(this).parent('.product').remove();
    });
    
    //Card number mask start
    var cardNum = document.getElementById('cr_no');
    cardNum.onkeyup = function (e) {
    if (this.value == this.lastValue) return;
    var caretPosition = this.selectionStart;
    var sanitizedValue = this.value.replace(/[^0-9]/gi, '');
    var parts = [];
    
    for (var i = 0, len = sanitizedValue.length; i < len; i += 4) {
        parts.push(sanitizedValue.substring(i, i + 4));
    }
    
    for (var i = caretPosition - 1; i >= 0; i--) {
        var c = this.value[i];
        if (c < '0' || c > '9') {
            caretPosition--;
        }
    }
    caretPosition += Math.floor(caretPosition / 4);
    
    this.value = this.lastValue = parts.join('-');
    this.selectionStart = this.selectionEnd = caretPosition;
    }
    //Card number mask end

    function make_payment(){
        $('#billing_address').submit();
    }

    $('#card-detail-tab').on('click',function(){
        $('#payment_type').val(1);
        // Payment type value is 1 => Payment with card
    });
    $('#account-detail-tab').on('click',function(){
        $('#payment_type').val(0);
        // Payment type value is 0 => Payment with Account number
    })
</script>
